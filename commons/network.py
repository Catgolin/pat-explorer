from asgiref import sync;
import flatdict;
from heurist import basic_queries;
import json;
import yaml;
import logging;
from rdflib import Graph, URIRef, FOAF;
import os;

logger = logging.getLogger(__name__)

if "async" == os.environ["async"]:
	import requests_async as requests;
else:
	import requests;


async def try_request(url, params, retry=1):
	full_url = get_full_url(url, params);
	if retry < 0:
		logger.error(f"Too many attempts: abort connexion to {full_url}");
		return (None, retry);

	try:
		if requests.__name__ == "requests_async":
			res = await requests.get(url=url, params=params);
		else:
			res = requests.get(url=url, params=params);
		if res.status_code in range(400,405):
			logger.error(f"Error {res.status_code} for {full_url}");
			return (None, -1);
	except requests.exceptions.ConnectionError:
		logger.info(f"Connexion to {full_url} failed: trying again...");
		res, retry = await try_request(url, params, retry - 1);
	except:
		logger.warning(f"Connexion to {full_url} failed for unclear reasons: trying again...");
		res, retry = await try_request(url, params, retry - 1);
	return (res, retry);

async def get_json(url, params, retry=1, field=None):
	full_url = get_full_url(url, params)
	if retry < 0:
		logger.error("Too many attempts: abort connexion to %s" % full_url)
		return None;

	try:
		(res, retry) = await try_request(url=url, params=params, retry=retry)
		if None == res:
			return None;
		data = res.json()
	except json.decoder.JSONDecodeError:
		logger.debug(res)
		if res.status_code in range(400,405):
			return None;
		logger.warning("There was an error in the results of %s : trying again..." % full_url)
		return await get_json(url, params, retry - 1, field)

	if not check_data_for_field(data, field):
		logger.debug(res)
		logger.warning("Incoherent response from %s : trying again..." % full_url)
		return await get_json(url, params, retry - 1, field)
	elif field:
		return data[field]
	return data

async def get_yaml(url, params, retry=1, field=None):
	full_url = get_full_url(url, params)
	if retry < 0:
		logger.error("Too many attempts: abort connexion to %s" % full_url)
		return None;

	try:
		(res, retry) = await try_request(url=url, params=params, retry=retry)
		if None == res:
			return None;
		data = yaml.safe_load(res.content)
	except yaml.scanner.ScannerError:
		logger.debug(res)
		logger.warning("There was a error in the results of %s : trying again..." % full_url)
		return await get_yaml(url, params, retry - 1, field)

	if not check_data_for_field(data, field):
		if 200 == res.status_code:
			logger.warning("No data for %s" % full_url)
			return None
		logger.warning("Incoherent response from %s : trying again..." % full_url)
		logger.debug(res)
		return await get_yaml(url, params, retry - 1, field)
	elif field:
		return data[field]
	return data

def check_data_for_field(data, field):
	if not data or len(data) < 1:
		return False
	if not field or field in data:
		return True
	if not field in data:
		logger.debug(data)
		return False

def get_full_url(url, params):
	full_url = url + "?"
	for param, value in params.items():
		full_url += param + "=" + value + "&"
	return full_url[:-1]


def get_graph(uri):
	g = Graph()
	try:
		g.parse(uri)
	except ConnectionError:
		# Retry once in case of connection error
		g.parse(uri)
	except HTTPError as e:
		if not e.code in (500, 502):
			logger.error(e)
			return
		g.parse(uri)
	return g;


def get_data_from_heurist_id(hid: int) -> dict:
	url = basic_queries.get_id_url(hid);
	raw_data = sync.async_to_sync(get_json)(url, {}, field="heurist");

	if not raw_data or not "records" in raw_data.keys() or len(raw_data["records"]) < 1:
		return None;

	data = raw_data["records"][0]["details"];
	return get_dictionary_from_heurist_list(*data);

def get_dictionary_from_heurist_list(*list_data) -> dict:
	data = {};
	for row in list_data:
		data[row["fieldName"]] = row["value"];
	flatten_data = flatdict.FlatDict(data, delimiter=".");
	return flatten_data;
