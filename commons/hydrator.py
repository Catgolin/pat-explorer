import re;

import logging;

from django.db import models;
from rdflib import Graph, URIRef, FOAF;
from rdflib.query import Result;

logger = logging.getLogger(__name__)

FOREIGN_SOURCES = {
	"data.cerl.org": {
		"Library": "get_holdinst_from_cerl",
	},
	"geonames.org": {
		"Place": "get_from_geonames",
	},
	"dishas.obspm.fr": {
		"TableType": "get_from_dishas",
	}
};


def get_object_from_source(model, source, **values):
	if not source in FOREIGN_SOURCES.keys():
		logger.error(f"Tried to fetch resource from unknown source {source}");
		return 501;
	if not model.__name__ in FOREIGN_SOURCES[source]:
		logger.error(f"Tried to fetch {model.__name__} from {source} while no method is defined for it");
		return 501;
	method_name = FOREIGN_SOURCES[source][model.__name__];
	try:
		method = getattr(model.objects, method_name);
	except AttributeError as e:
		logger.error(e);
		return None;
	result = method(**values);
	if None == result:
		logger.error(f"{model.__name__}.objects.{method_name} didn't return a value for {values}");
	return result;


def update_model_from_source_data(model, source, force_update = False, **data):
	for key, value in data.items():
		set_property_from_source(model, source, key, value, force_update);


def set_property_from_source(model, source, data_name, value, force_update = False):
	property_name = data_name if None == source else get_property_name_from_source(model, source, data_name);

	if None == property_name:
		return False;

	property_meta = model.__class__._meta.get_field(property_name);

	if isinstance(property_meta, models.ManyToManyField):
		return add_value_to_property_from_source(model, source, property_name, value);

	current_value = getattr(model, property_name);

	if current_value and True != force_update:
		logger.debug("Value {value} was not updated from {source} for attribute {property_name} because it was already set to {current_value}");
		return False;

	if isinstance(property_meta, models.ForeignKey):
		value =  get_foreign_key_from_source(model, source, data_name, value, force_update = force_update);
		if not value:
			return False;

	if not update_property(model, property_name, value):
		return False;

	update_source_of_model(model, source, property_name);

	return True;

def update_property(self, property_name, value):
	# if hasattr(self, f"set_{property_name}"):
	# 	updated = getattr(self, f"set_{property_name}")(value, source=source);
	# 	if False == updated:
	# 		return False;
	# 	return True;

	setattr(self, property_name, value);

	return True;

def add_value_to_property_from_source(self, source, property_name, value):
	pass;

def get_foreign_key_from_source(self, source, data_name, value, force_update = False):
	property_name = get_property_name_from_source(self, source, data_name);
	if None == property_name:
		logger.error(f"There is no property associated to {data_name} for {source}");
		return False;

	property_meta = self.__class__._meta.get_field(property_name);
	foreign_model = property_meta.remote_field.model;
	foreign_sources = foreign_model.ontology();

	if not source in foreign_sources["sources"].keys():
		logger.error(f"Source {source} is not defined for {foreign_model.__name__}");
		return None;

	if not data_name in foreign_sources["sources"][source].keys():
		logger.info(f"Failed to update unknown attribute {data_name} on {foreign_model.__name__} from {source}");
		return None;

	foreign_attribute = foreign_sources["sources"][source][data_name]

	if foreign_attribute in foreign_sources["keys"].keys():
		values = {foreign_attribute: value};
		return get_object_from_source(foreign_model, foreign_sources["keys"][foreign_attribute], **values);
	logger.error(f"There is no method to create a {foreign_model.__name__} with {foreign_attribute}");
	return None;


def update_source_of_model(self, source, property_name):
	current_source_information = check_for_source_attribute(self);
	if False == current_source_information:
		return False;

	previous_source = get_source_of_property(self, property_name)
	if None == previous_source:
		source = current_source_information + f"\n{property_name}: {source}";
	else:
		regexp = f"{property_name}( )*:( )*{source}";
		source = re.sub(regexp, f"{property_name}: {source}", current_source_information);
	setattr(self, "data_sources", source);
	self.save();


def get_source_of_property(self, property_name):
	source = check_for_source_attribute(self);
	if False == source:
		return None;

	for line in source.split("\n"):
		line = line.split(":");
		if 2 != len(line):
			continue;
		key, value = line;
		if key.strip() == property_name.strip():
			return value;
	return None;


def get_property_name_from_source(self, source, data_name):
	model = self.__class__.__name__;
	if not source in self.__class__.ontology()["sources"].keys():
		logger.error(f"Tried to update a {model} from unknown source {source}");
		return None;
	if not data_name in self.__class__.ontology()["sources"][source].keys():
		logger.debug(f"Failed to update unknown attribute {data_name} on {model} from {source}");
		return None;

	property_name = self.__class__.ontology()["sources"][source][data_name];
	try:
		current_value = getattr(self, property_name);
	except AttributeError as e:
		logger.error(e);
		return None;
	return property_name;

def check_for_source_attribute(self):
	if not hasattr(self, "data_sources"):
		return False;

	if not getattr(self, "data_sources"):
		setattr(self, "data_sources", "");

	return getattr(self, "data_sources");


def get_properties_from_graph(graph, key, model, source):
	ontology = model.ontology()["sources"][source];

	query = build_sparql_query(key, ontology, source);
	return sparql_result_to_dict(graph.query(query), ontology);

def get_model_data_from_sparql(model, source, endpoint, key):
	ontology = model.ontology()["sources"][source];

	query = build_sparql_query(key, ontology, source, endpoint = endpoint);
	g = Graph();
	return sparql_result_to_dict(g.query(query), ontology);


def build_sparql_query(key: str, ontology: dict, source: str, endpoint: str = "") -> str:
	prefixes = get_sparql_prefixes(source);
	service = f"SERVICE <{endpoint}>" if endpoint else "";
	fields = " ".join([f"?{value}" for value in ontology.values()]);
	query_items = [f"{query} ?{value}".format(key=key) for query, value in ontology.items()];
	query = "} UNION {".join(query_items);
	return (
		f"{prefixes} SELECT {fields} "
		"WHERE {"
		f"{service}"
		"{{"
		f"{query}"
		"}}}"
	);

def get_sparql_prefixes(source: str) -> str:
	prefixes = (
		"PREFIX foaf: <http://xmlns.com/foaf/0.1/> "
		"PREFIX sdo: <http://schema.org/> "
	);
	if "data.bnf.fr" == source:
		prefixes += "PREFIX bio: <http://vocab.org/bio/0.1/> ";
	else:
		prefixes += "PREFIX bio: <http://purl.org/vocab/bio/0.1/> "
	return prefixes;

def sparql_result_to_dict(res: Result, ontology: dict) -> dict:
	data = {};
	for row in res:
		i, value = [(i, value) for i, value in enumerate(row) if None != value][0]
		property_name = [query for query, name in ontology.items()][i]
		data[property_name] = value.__str__();
	return data;
