from django.db.models import Model;
from django.test import TestCase

def assert_data_sourced(model: Model, sourced_properties: list, source: str):
	actual_sources = model.data_sources.split("\n");
	for key in sourced_properties:
		message = f"{source} is not indicated as source for {key} in {actual_sources}"
		assert f"{key}: {source}" in actual_sources, message;
