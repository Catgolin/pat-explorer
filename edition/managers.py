from django.db.models import Manager;

class TableManager(Manager):
	def all(self, *args, **kwargs):
		return super().all(*args, **kwargs).order_by("page_start");
