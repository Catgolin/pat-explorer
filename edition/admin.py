from django.contrib import admin;

from .models import Table;
from astronomical_contents.models import TableType;

@admin.register(Table)
class TableAdmin(admin.ModelAdmin):
	save_as = True;
	list_display = ('__str__', 'table_type', 'witness');

	def formfield_for_foreignkey(self, db_field, request, **kwargs):
		if 'table_type' == db_field.name:
			kwargs["queryset"] = TableType.objects.order_by("astronomical_object");
		return super().formfield_for_foreignkey(db_field, request, **kwargs);
