import asyncio;

from django.db import models;
from django.utils.translation import gettext_lazy as _;

from commons import network;
from source_materials.models import Witness;
from astronomical_contents.models import TableType;

from .managers import TableManager;

class Table(models.Model):
	""" Representation of one paricular table in one particlar witness """

	objects = TableManager();

	witness = models.ForeignKey(
		Witness,
		verbose_name = _("witness"),
		on_delete=models.CASCADE,
		help_text = _("The witness from which this table can be read"),
	);
	table_type = models.ForeignKey(
		TableType,
		verbose_name = _("type"),
		on_delete=models.CASCADE,
		null=True, blank=True,
		help_text = _("The type of this table"),
	);
	original_title = models.CharField(
		_("original title"),
		max_length = 200,
		blank=True,
		help_text = _("The title of this table in the original witness")
	);
	# language = models.CharField(max_length = 20, null=True, blank=True, default="Latin");
	page_start = models.IntegerField(
		_("first page"),
		null=True, blank=True,
		help_text = _("Index of the first page of the witness containing this table (set for digital witnesses in priority)"),
	);
	page_end = models.IntegerField(
		_("last page"),
		null=True, blank=True,
		help_text = _("Index of the last page of this witness containing this table (set for digital witnesses in priority)"),
	);
	folio_start = models.CharField(
		_("first folio"),
		max_length = 10,
		blank=True,
		help_text = _("Name of the first folio containing this table in the witness (ie 'A1r')"),
	);
	folio_end = models.CharField(
		_("last folio"),
		max_length = 10,
		blank=True,
		help_text = _("Name of the last folio containing this table in the witness (ie 'A1v')"),
	);
	notes = models.TextField(
		_("notes"),
		blank=True,
		help_text = _("Custom notes about this particular table"),
	);

	def __str__(self):
		if self.original_title:
			return self.original_title
		return f"{self.table_type.__str__()} in {self.witness.__str__()}";

	def get_images_url(self):
		if not self.witness.iiif_manifest:
			return [];
		manifest = asyncio.run(network.get_json(self.witness.iiif_manifest, {}));
		if None == manifest:
			return [];
		canvases = manifest['sequences'][0]['canvases'];
		pages = canvases[self.page_start-1:self.page_end];
		meta_images = [p['images'][0]['resource']['service']['@id'] for p in pages];
		images = [f"{url}/full/full/0/default.jpg" for url in meta_images];
		return images;
