from django.shortcuts import render, get_object_or_404;
from django.views import generic;
from django.utils.translation import gettext_lazy as _;
from django.core.exceptions import ObjectDoesNotExist as DoesNotExist;

import logging;

from works.models import Title, Corpus;
from source_materials.models import Witness;
from .models import Table;

logger = logging.getLogger(__name__);

class CorpusIndexView(generic.ListView):
	context_object_name = "corpuses_list";
	template_name = "edition/index.html"

	def get_queryset(self):
		return Corpus.objects.all().order_by("composition_date", "name");

def titles_index(request, pk = None):
	if None == pk:
		context = {
			"corpus_name": _("No corpus"),
			"titles_list": Title.objects.filter(corpus__isnull=True).order_by("print_date"),
		};
		return render(request, "edition/list_tables_in_corpus.html", context);
	corpus = get_object_or_404(Corpus, pk=pk);
	context = {
		"corpus_name": corpus.__str__(),
		"corpus": corpus,
		"titles_list": corpus.titles.order_by("print_date"),
	}
	return render(request, "edition/list_tables_in_corpus.html", context);


class WitnessDetailView(generic.DetailView):
	model = Witness;
	template_name = "edition/witness_detail.html";

class TableDetailView(generic.DetailView):
	model = Table;

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs);
		table = context["table"];
		context["other_tables"] = Table.objects.filter(table_type=table.table_type).exclude(pk=table.pk).all();
		all_tables = Table.objects.filter(witness=table.witness).order_by("page_start");
		neighbors = list(all_tables.all());
		i = neighbors.index(table);
		previous_table = neighbors[i-1] if i > 0 else None;
		next_table = neighbors[i+1] if i + 1 < len(neighbors) else None;
		context["previous_table"] = previous_table;
		context["next_table"] = next_table;
		return context;

def compare_tables(request, first, second=None):
	context = {};
	table = get_object_or_404(Table, pk=first);
	context["table"] = table;

	if None == second:
		try:
			second = request.GET["to"];
		except KeyError:
			pass;

	try:
		context["compare"] = Table.objects.get(pk=second);
	except DoesNotExist:
		pass;
	context["other_tables"] = Table.objects.filter(table_type=table.table_type).exclude(pk=table.pk).all();
	return render(request, "edition/table_compare.html", context);

def compare_witness(request, first, second):
	context = {
		"first_witness": get_object_or_404(Witness, pk=first),
		"second_witness": get_object_or_404(Witness, pk=second),
	};
	return render(request, "edition/witness_compare.html", context);
