from unittest import skip;

from django.test import TestCase

from commons.tests import assert_data_sourced;
from source_materials.models import Witness;
from astronomical_contents.models import TableType;

from .models import Table;

class TableTest(TestCase):
	def test_get_pages_from_iiif(self):
		sources = Witness.objects.get_digital_reproductions_from_istc("ia00534000");
		witness = sources[0];
		witness.iiif_manifest = 'https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb00060314/manifest';
		witness.save();
		week_days = TableType(
			name="Week days",
			chabas_number=4,
			chabas_description_page_start=243,
			chabas_description_page_end=243
		);
		table = Table(
			witness = witness,
			original_title="Tabule astronomice illustrissimi Alfonsii regis castelle incipiut sidere foelici",
			page_start=45,
			folio_start = "c7r",
			page_end=45,
			folio_end="c7r",
		);
		images_links = table.get_images_url();
		expected_links = [
			'https://api.digitale-sammlungen.de/iiif/image/v2/bsb00060314_00045/full/full/0/default.jpg',
		];
		self.assertListEqual(images_links, expected_links);

	def test_table_from_source_multiple_pages(self):
		sources = Witness.objects.get_digital_reproductions_from_istc("ia00534000");
		witness = sources[0];
		witness.iiif_manifest = 'https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb00060314/manifest';
		witness.save();
		solar_equation = TableType.objects.get_from_dishas(4);
		table = Table(
			witness = witness,
			table_type = solar_equation,
			original_title="Tabula equationis solis",
			page_start=68,
			page_end=70,
			folio_start="e2v",
			folio_end="e3v",
		);
		images_links = table.get_images_url();
		expected_links = [
			'https://api.digitale-sammlungen.de/iiif/image/v2/bsb00060314_00068/full/full/0/default.jpg',
			'https://api.digitale-sammlungen.de/iiif/image/v2/bsb00060314_00069/full/full/0/default.jpg',
			'https://api.digitale-sammlungen.de/iiif/image/v2/bsb00060314_00070/full/full/0/default.jpg',
		];
		self.assertListEqual(images_links, expected_links);
