# Generated by Django 4.2 on 2023-04-25 10:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('edition', '0001_initial'),
        ('source_materials', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='table',
            name='witness',
            field=models.ForeignKey(help_text='The witness from which this table can be read', on_delete=django.db.models.deletion.CASCADE, to='source_materials.witness', verbose_name='witness'),
        ),
    ]
