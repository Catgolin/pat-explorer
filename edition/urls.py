from django.urls import path;

from . import views;

app_name = "edition";

urlpatterns = [
	path('', views.CorpusIndexView.as_view(), name="index"),
	path('corpus/<int:pk>', views.titles_index, name="corpus_index"),
	path('corpus', views.titles_index, name="orphans_index"),
	path('source/<int:pk>', views.WitnessDetailView.as_view(), name="witness"),
	path('table/<int:pk>', views.TableDetailView.as_view(), name="table"),
	path('tables/compare/<int:first>/<int:second>', views.compare_tables, name="compare-tables"),
	path('tables/compare/<int:first>', views.compare_tables, name="compare-tables"),
	path('compare/<int:first>/<int:second>', views.compare_witness, name="compare-witness"),
];
