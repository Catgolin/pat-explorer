from django.views import generic;

from .models import Actor, Place;

class ActorsIndexView(generic.ListView):
	context_object_name = "actors_list";

	def get_queryset(self):
		return Actor.objects.all().order_by("name", "family_name", "given_name");

class ActorDetailView(generic.DetailView):
	model = Actor;

class PlaceDetailView(generic.DetailView):
	model = Place;
