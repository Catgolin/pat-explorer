from django.urls import path;

from . import views;

app_name = "scenery";

urlpatterns = [
	path('actors', views.ActorsIndexView.as_view(), name="index"),
	path('actor/<int:pk>', views.ActorDetailView.as_view(), name="actor"),
	path('place/<int:pk>', views.PlaceDetailView.as_view(), name="place"),
];
