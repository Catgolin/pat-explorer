import logging;

from django.db import models
from django.utils.translation import gettext_lazy as _

from . import managers;

logger = logging.getLogger(__name__);

class Actor(models.Model):
	@staticmethod
	def ontology():
		return {
			"keys": {
				"viaf_id": "viaf.org",
				"idref_id": "idref.fr",
			},
			"sources": {
				"data.idref.fr": {
					"<http://www.idref.fr/{key}/id> foaf:name": "name",
					"<http://www.idref.fr/{key}/id> foaf:familyName": "family_name",
					"<http://www.idref.fr/{key}/id> foaf:givenName": "given_name",
					"<http://www.idref.fr/{key}/birth> bio:date": "birth_date",
					"<http://www.idref.fr/{key}/death> bio:date": "death_date",
				},
				"viaf.org": {
					"<http://viaf.org/viaf/{key}> sdo:name": "name",
					"<http://viaf.org/viaf/{key}> sdo:givenName": "given_name",
					"<http://viaf.org/viaf/{key}> sdo:familyName": "family_name",
					"<http://viaf.org/viaf/{key}> sdo:birthDate": "birth_date",
					"<http://viaf.org/viaf/{key}> sdo:deathDate": "death_date",
				},
				"data.bnf.fr": {
					"<http://data.bnf.fr/ark:/{key}#about> foaf:name": "name",
					"<http://data.bnf.fr/ark:/{key}#about> foaf:givenName": "given_name",
					"<http://data.bnf.fr/ark:/{key}#about> foaf:familyName": "family_name",
					"<http://data.bnf.fr/ark:/{key}#about> bio:birth": "birth_date",
					"<http://data.bnf.fr/ark:/{key}#about> bio:death": "death_date",
				},
			},
		};


	objects = managers.ActorManager();

	class Meta:
		verbose_name = _("actor");
		verbose_name_plural = _("actors");


	#   ===   Database identifiers   ===
	viaf_id = models.CharField(
		_("Viaf identifier"),
		max_length=100,
		null=True, unique=True,
		help_text = _("Identifier of the actor in OCLC's viaf.org database"),
	);
	dishas_id = models.CharField(
		_("Dishas' identifier"),
		max_length=200,
		null=True, blank=True, unique=True,
		help_text = _("Identifier of the actor in the dishas.obspm.fr's database"),
	)
	ppn = models.CharField(
		_("Pica production number"),
		max_length=9,
		null=True, blank=True, unique=True,
		help_text=_("9-digits number identificator for Sudoc and Abes-related databases"),
	);
	ark = models.CharField(
		_("archival Resource Key"),
		max_length = 50,
		null = True, blank = True, unique = True,
		help_text = _("Archival Resource Key, likely from the BNF (not prefixed by the `ark:/` keyword)"),
	);

	#   ===   Names   ===
	given_name = models.CharField(
		_("given name"),
		max_length=200,
		blank=True,
		help_text = _("First name of the Actor (if any)"),
	);
	family_name = models.CharField(
		_("family name"),
		max_length=200,
		blank=True,
		help_text = _("Family name of the Actor (if any)"),
	);
	name = models.CharField(
		_("full name"),
		max_length=200,
		blank=True,
		help_text = _("Complete name of the Actor (generaly of the form `[given_name] [family_name]`)"),
	);

	#   ===   Chronology   ===
	birth_date = models.CharField(
		_("date of birth"),
		max_length=10,
		blank=True,
		help_text = _("Date of bith of the Actor (either of the form `[YYYY]-[MM]-[DD]` or `[YYYY]`)"),
		# Note for later:
		# It could happen that indications are added to the date (ie "circa", etc.),
		# but it would be great if this kind of informations was stored in a separate attribute
	);
	death_date = models.CharField(
		_("date of death"),
		max_length=10,
		blank=True,
		help_text = _("Date of death of the Actor (either of the form `[YYYY]-[MM]-[DD]` or `[YYYY]`)"),
		# Note for later:
		# It could happen that indications are added to the date (ie "circa", etc.),
		# but it would be great if this kind of informations was stored in a separate attribute
	);

	same_as = models.ManyToManyField(
		'self',
		verbose_name=_("equivalent resource"),
		help_text=_("Link different descriptions of the same historical Actor."),
	);

	#   ===   Sources   ===
	data_sources = models.TextField(
		_("sources of the informations"),
		blank=True,
		help_text = _("Source of the informations stored about this actor"),
	);

	def __str__(self) -> str:
		return self.full_name;

	@property
	def full_name(self) -> str:
		if self.name:
			return self.name;
		names = [];
		if self.given_name:
			names.append(self.given_name);
		if self.family_name:
			names.append(self.family_name);
		return " ".join(names);

	@property
	def dates(self) -> str:
		if self.birth_year and self.death_year:
			return f"{self.birth_year}--{self.death_year}";
		if self.birth_year:
			return f"b.{self.birth_year}";
		if self.death_year:
			return f"d.{self.death_year}";
		return "";

	@property
	def birth_year(self) -> str:
		if self.birth_date and len(self.birth_date) > 3:
			return self.birth_date[0:4];
		return "";

	@property
	def death_year(self) -> str:
		if self.death_date and len(self.death_date) > 3:
			return self.death_date[0:4];
		return "";


class Place(models.Model):
	@staticmethod
	def ontology():
		return {
			"keys": {
				"geonames_id": "geonames.org",
			},
			"sources": {
				"data.cerl.org": {
					"geonamesId": "geonames_id",
					"imprint.geo_info.geonames_id": "geonames_id",
				},
				"geonames.org": {
					"toponymName": "name",
					"lat": "latitude",
					"long": "longitude"
				},
			},
		};


	objects = managers.PlaceManager()

	class Meta:
		verbose_name = _("place");
		verbose_name_plural = _("places");


	#   ===   Database   ===
	dishas_id = models.CharField(
		_("Dishas' identifier"),
		max_length=200,
		null=True, blank=True, unique=True,
		help_text=_("Identifier of the place in the dishas.obspm.fr database"),
	);
	geonames_id = models.IntegerField(
		_("Geonames' identifier"),
		null=True, blank=True, unique=True,
		help_text=_("Identifier of the place in the geonames.org database"),
	);

	#   ===   Geography   ===
	latitude = models.FloatField(
		_("latitude"),
		null=True, blank=True,
		help_text=_("Angle between the equator and the parallel of the place"),
		# Note for later:
		# It would be nice to check that the latitude is between -90 and 90
	);
	longitude = models.FloatField(
		_("longitude"),
		null=True, blank=True,
		help_text=_("Angle between the meridian of the place and the Prime meridian"),
		# Note for later:
		# It would be nice to check that the latitude is between -180 and 180
	);

	#   ===   Names   ===
	name  = models.CharField(
		_("name"),
		max_length=200,
		blank=True,
		help_text=_("Name of the place"),
	);

	#   ===   Sources   ===
	data_sources = models.TextField(
		_("sources of the informations"),
		blank=True,
		help_text = _("Source of the informations stored about this place"),
	);

	def __str__(self):
		return self.name

