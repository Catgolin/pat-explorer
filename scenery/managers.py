import asyncio;
import re;
import geocoder;
import os;

import logging;

from fuzzywuzzy import fuzz;
from rdflib import URIRef;
from urllib.parse import urlparse;

from django.db.models import Manager;

from commons import network, hydrator;

logger = logging.getLogger(__name__);

class ActorManager(Manager):

	# Dictionnary where the keys are the name of the sources, and values are couples:
	# The first element returns the method to get an `Actor` from this source.
	# The second element is the name of the key used to identify a resource from that source.
	# Elements in the dictionnary should be inserted in order of preference:
	# the first source of the list is the one that should always be used if it is available,
	# the last one should only be used if no other is available.
	prefered_sources = {
		"data.idref.fr": (lambda self: self.get_from_idref, "ppn"),
		"data.bnf.fr": (lambda self: self.get_from_bnf, "ark"),
		"viaf.org": (lambda self: self.get_from_viaf, "viaf_id"),
	};

	frequent_orthograph_variations = {
		("es ", "is "),
		("is ", "es "),
		("ph", "f"),
		("f", "ph"),
		("us ", "o "),
		("o ", "us "),
		("y", "i"),
	};

	# TODO: Documentation
	# Note: This is quite slow to get results from viaf.org
	def get_from_names(self, names: str, activity_date: int = 1641, alive_while_active: bool = False, separators: list[str]=["and", "\n"]) -> list:
		for separator in separators:
			names = names.replace(separator, separators[0]);
		names_list = [name.strip() for name in names.split(separators[0]) if name.strip()];
		actors = [];
		for name in names_list:
			actor = self.get_from_name(name, activity_date, alive_while_active);
			if None == actor:
				continue;
			actors.append(actor);
		return actors;

	# TODO: Documentation
	def get_from_name(self, name: str, activity_date: int = 1641, alive_while_active: bool = False):
		if "s.publ." == name:
			return None;
		if not name or len(name) < 3:
			return None;

		try:
			return self.get(name=name);
		except self.model.DoesNotExist as e:
			pass;

		viaf_id = self.get_viaf_id_from_name(name, activity_date, alive_while_active);
		if None == viaf_id:
			return self.get_or_create(name = name)[0];
		actor = self.get_from_viaf(viaf_id);
		hydrator.update_source_of_model(actor, "viaf.org", "viaf_id");
		return actor;

	# TODO: Documentation
	def get_viaf_id_from_name(self, name: str, activity_date: int = None, alive_while_active: bool = True):
		possible_variant_names = [name];
		for orthograph_variation in self.frequent_orthograph_variations:
			if not orthograph_variation[0] in name:
				continue;
			variant_name = name.replace(orthograph_variation[0], orthograph_variation[1]);
			possible_variant_names.append(variant_name);

		rated_data = asyncio.run(self.search_and_rate_name_variants(possible_variant_names, activity_date, alive_while_active));
		rated_data.sort(key = lambda t: t[1]);
		if len(rated_data) < 1:
			logger.info(f"The search for '{name}' returned no result in viaf.org (tried {possible_variant_names}).");
			return None;
		if rated_data[-1][1] < 1:
			logger.info(f"No data from the search for '{name}' was pertinent in viaf.org (max score: {rated_data[-1][1]}).");
			return None;
		return rated_data[-1][0];

	async def search_and_rate_name_variants(self, possible_variant_names, activity_date, alive_while_active):
		list_of_rated_data = await asyncio.gather(*[
			self.search_and_grade_viaf_results(name_variation, activity_date, alive_while_active)
			for name_variation in possible_variant_names
		]);
		return [rated_data for rated_list in list_of_rated_data for rated_data in rated_list];

	# TODO: Documentation
	async def search_and_grade_viaf_results(self, name: str, activity_date: int = None, alive_while_active: bool = True) -> list[tuple[str, float]]:
		url = "https://viaf.org/viaf/search";
		params = {
			"query": f"local.personalNames all \"{name}\"",
			"sortKeys": "holdingscount",
			"format": "BriefVIAFCluster",
			"httpAccept": "application/json",
		};
		raw_data = await network.get_json(url, params, field="searchRetrieveResponse");
		if None == raw_data:
			return None;
		rated_data = [];
		if not "records" in raw_data:
			logger.debug(f"No data in viaf search for {name}");
			return [];
		for row in raw_data["records"]:
			row_data = row["record"]["recordData"];
			row_data["birth_date"] = row_data["birthDate"];
			row_data["death_date"] = row_data["deathDate"];
			rated_data.append((
				row_data["viafID"],
				self.rate_search_result(row_data, name, activity_date, alive_while_active),
			));
		return rated_data;

	# TODO: Documentation
	def rate_search_result(self, result: dict, name: str, activity_date: int = None, alive_while_active: bool = True) -> float:
		initial_score = len(result["sources"]["source"]);
		name_score = self.rate_name_from_search_result(result, name);
		if name_score < 1:
			return 0;
		score = initial_score + name_score;
		final_score = self.rate_dates_from_search_result(result, activity_date, alive_while_active, score);
		# Usefull print for debuging
		# logger.debug(f"Rated {result['viafID']} for search '{name}':\n- {initial_score} for number of sources,\n- {name_score} for matching names,\n- {final_score} total");
		return final_score;

	# TODO: Documentation
	def rate_name_from_search_result(self, result: dict, name: str, initial_score: float = 0) -> float:
		separate_searched_names = name.replace(',', '').split(' ');
		result_names = result['mainHeadings']['data'];
		names_list = [];
		if isinstance(result_names, dict):
			names_list = [result_names['text']];
		else:
			names_list = [res['text'] for res in result_names];

		score = initial_score;
		for searched_name in separate_searched_names:
			match = self.is_name_in_result(names_list, searched_name);
			if match > 50:
				score += match;
		return score / len(separate_searched_names);

	# TODO: Documentation
	def is_name_in_result(self, result_names: list[str], name: str) -> int:
		for result_name in result_names:
			separate_result_names = result_name.replace(',', '').split(' ');
			for separate_name in separate_result_names:
				match = fuzz.ratio(name, separate_name);
				if match > 50:
					return (match - 50) * 2;
		return 0;

	# TODO: Documentation
	def rate_dates_from_search_result(self, result: dict, activity_date: int = None, alive_while_active: bool = True, initial_score: int = 0):
		activity_date = get_year_from_string(activity_date);
		birth = get_year_from_string(result["birth_date"]);
		death = get_year_from_string(result["death_date"]);
		if activity_date:
			activity_date = int(activity_date);

		if activity_date and birth and activity_date < birth:
			return -1;
		if activity_date and death and alive_while_active and death < activity_date:
			return -1;
		return initial_score;


	def get_from_viaf(self, viaf_id: str, force: bool = False):
		""" Creates an actor using informations from OCLC's viaf.org
		If the viaf database links to resources describing the same author
		from sources listed in the `prefered_sources` attribute,
		those sources will be used instead """
		if not viaf_id:
			return None;

		uri = f"http://viaf.org/viaf/{viaf_id}";
		alternatives = self.get_sameAs(uri);
		for source, (factory, key_name) in self.prefered_sources.items():
			if source in alternatives.keys():
				actor = factory(self)(alternatives[source]);
				hydrator.update_source_of_model(actor, "viaf.org", key_name);
				actor.viaf_id = viaf_id;
				return actor;

		source, data = self.get_informations_from_viaf(viaf_id);

		if not data:
			return None;

		actor = self.get_or_create(viaf_id = viaf_id)[0];
		hydrator.update_model_from_source_data(actor, source, force_update = force, **data);
		return actor;

	def get_from_idref(self, ppn: str, force: bool = False):
		"""" Creates an actor using informations from Abes' data.idref.fr """
		if not ppn:
			return None;

		data = self.get_informations_from_idref(ppn);

		if not data:
			return None;

		actor = self.get_or_create(ppn=ppn)[0]
		hydrator.update_model_from_source_data(actor, data[0], force_update = force, **data[1]);
		actor.ppn = ppn;

		return actor;

	def get_from_bnf(self, ark: str, force: bool = False):
		""" Creates an actor using informations from data.bnf.fr """
		if not ark:
			return None;

		data = self.get_informations_from_bnf(ark);

		if not data:
			return None;

		actor = self.get_or_create(ark=ark)[0]
		hydrator.update_model_from_source_data(actor, data[0], force_update = force, **data[1]);
		actor.ark = ark;

		return actor;


	def get_informations_from_viaf(self, viaf_id: str) -> tuple[str, dict]:
		""" Returns informations about an actor taken from the OCLC's viaf.org database
		In the returned tuple:
		the first element indicates the source of the informations,
		the second element is a dictionary representing the informations about the actor """
		if not viaf_id:
			return False;

		uri = f"http://viaf.org/viaf/{viaf_id}";
		g = network.get_graph(uri);
		subject = URIRef(uri);
		return (
			"viaf.org",
			hydrator.get_properties_from_graph(g, viaf_id, self.model, "viaf.org")
		);

	def get_informations_from_idref(self, ppn: str) -> tuple[str, dict]:
		""" Returns informations about an actor taken from the Abes' idref database
		In the returned tuple:
		the first element indicates the source of the informations,
		the second element is a dictionary representing the informations about the actor """
		if not ppn:
			return False;

		endpoint = "https://data.idref.fr/sparql";

		data = hydrator.get_model_data_from_sparql(self.model, "data.idref.fr", endpoint, ppn);

		if None == data:
			logger.warning(f"No data for idref ppn {ppn}");
			return None;

		result = {};

		for key in self.model.ontology()["sources"]["data.idref.fr"].keys():
			if key in data:
				result[key] = data[key];

		return ('data.idref.fr', result);

	def get_informations_from_bnf(self, ark: str) -> tuple[str, dict]:
		""" Returns informations about an actor taken from the data.bnf.fr database
		In the returned tuple:
		the first element indicates the source of the informations,
		the second element is a dictionary representing the informations about the actor """
		if not ark:
			return False;

		endpoint = "https://data.bnf.fr/sparql";

		data = hydrator.get_model_data_from_sparql(self.model, "data.bnf.fr", endpoint, ark);

		if None == data:
			logger.warning(f"No data for bnf ark {bnf}");
			return None;

		result = {};

		for key in self.model.ontology()["sources"]["data.bnf.fr"].keys():
			if key in data:
				result[key] = data[key];

		return ("data.bnf.fr", result);


	def get_sameAs(self, uri: str, source: str = "") -> dict[str]:
		""" Returns a list of resources that are described by the `source`
		as identical to the resource identified by the `uri`.

		If no `source` is given, the `uri` will be used a `source`.
		Otherwise, `source` is the address where a rdf graph describing the resource can be found.

		The dictionnary returned by the function associate a source to the key of the resource
		described by the source (ie the ppn for idref, or the ark for data.bnf.org) """
		source = source if source else uri;
		g = network.get_graph(source);
		subject = URIRef(uri);
		sameAs = URIRef("http://schema.org/sameAs");
		resources = {};
		for s, p, o in g.triples((subject, sameAs, None)):
			source, key = self.get_source_from_uri(o.__str__());
			resources[source] = key;
		return resources;

	def get_source_from_uri(self, uri: str) -> tuple[str, str]:
		""" Returns the informations needed to build the dictionnary returned by the `sameAs` method
		(See `sameAs`) """
		for (stuff, key_name) in self.prefered_sources.values():
			key = getattr(self, f"get_{key_name}_from_uri")(uri);
			if key:
				return key;
		url = urlparse(uri);
		return (url.hostname, url.path);

	def get_viaf_id_from_uri(self, uri: str) -> tuple[str, str]:
		""" Returns the viaf identifier of the resource described by the provided `uri`,
		along with an indication of the source associated with the key (viaf.org).
		If `uri` doesn't point to a resource identified by a viaf identifier, returns None """
		url = urlparse(uri);
		if "viaf.org" == url.hostname:
			return ("viaf.org", url.path.strip("/").split("/")[1]);
		return None;

	def get_ppn_from_uri(self, uri: str) -> tuple[str, str]:
		""" Returns the ppn of the resource described by the provided `uri`,
		along with an indication of the source associated with the key.
		If `uri` doesn't point to a resource identified by a ppn, returns None """
		url = urlparse(uri);
		if "www.idref.fr" == url.hostname:
			return ("data.idref.fr", url.path.strip("/").split("/")[0])
		return None;

	def get_ark_from_uri(self, uri: str) -> str:
		""" Returns the archival resource key of the resource described by the provided `uri`,
		along with an indication of the source associated with the key.
		If `uri` doesn't point to a resource identified by an ark, returns None """
		match = re.search("ark:/(\d+/.*)", uri.split("#")[0])
		if match:
			return (urlparse(uri).hostname, match.group(1));
		return match;


class PlaceManager(Manager):

	def get_from_geonames(self, geonames_id: int):
		""" Creates a Place using informations from geonames.org """
		if not geonames_id:
			return;
		place = self.get_or_create(geonames_id = geonames_id) [0];
		data = self.get_data_from_geonames(geonames_id);
		if None == data:
			return None;
		hydrator.update_model_from_source_data(place, data[0], **data[1]);
		return place;


	def get_data_from_geonames(self, geonames_id: int) -> tuple[str, dict]:
		""" Returns informations about a Place taken from the geonames' database
		In the returned tuple:
		the first element indicates the source of the informations,
		the second element is a dictionary representing the informations about the Place """
		if not geonames_id:
			return;

		g = geocoder.geonames(geonames_id, method='details', key=os.environ["geonames_key"]);
		if None == g:
			logger.info(f"No results from geonames for id {geonames_id}");
			return None;
		return (
			"geonames.org",
			{
				"toponymName": g.address,
				"long": float(g.lng) if None != g.lng else None,
				"lat": float(g.lat) if None != g.lat else None,
			}
		);


# TODO: Documentation
def get_year_from_string(string: str, number_of_digits: int=4):
	if type(1) == type(string):
		return string;

	if number_of_digits < 1:
		return None;

	if len(string) < number_of_digits:
		return None;

	try:
		return int(string[0:number_of_digits]);
	except ValueError:
		return get_year_from_string(string, number_of_digits - 1);
