from unittest import skip, expectedFailure;

from django.test import TestCase;

from .models import Actor, Place;

from commons.tests import assert_data_sourced;

class ActorTest(TestCase):
	def test_get_actor_from_idref(self):
		actor = Actor.objects.get_from_idref("095008152");
		self.assertIsNotNone(actor.id);
		self.assertEqual(actor.name, "Erhard Ratdolt");
		self.assertEqual(actor.given_name, "Erhard");
		self.assertEqual(actor.family_name, "Ratdolt");
		self.assertEqual(actor.birth_date, "1447");
		self.assertEqual(actor.death_date, "1528");
		self.assertEqual(actor.ppn, "095008152");
		sourced_properties = ["name", "family_name", "given_name", "birth_date", "death_date"];
		assert_data_sourced(actor, sourced_properties, "data.idref.fr");

	def test_get_actor_from_idref_with_missing_elements(self):
		actor = Actor.objects.get_from_idref("027460959");
		self.assertIsNotNone(actor.id);
		self.assertEqual(actor.name, "Alphonse X");
		self.assertEqual(actor.given_name, "");
		self.assertEqual(actor.family_name, "");
		self.assertEqual(actor.birth_date, "1221-11-26");
		self.assertEqual(actor.death_date, "1284-04-04");
		self.assertEqual(actor.ppn, "027460959");
		sourced_properties = ["name", "birth_date", "death_date"];
		assert_data_sourced(actor, sourced_properties, "data.idref.fr");

	def test_get_informations_from_idref(self):
		data = Actor.objects.get_informations_from_idref("095008152");
		expected_data = {
			"<http://www.idref.fr/{key}/id> foaf:name": "Erhard Ratdolt",
			"<http://www.idref.fr/{key}/id> foaf:givenName": "Erhard",
			"<http://www.idref.fr/{key}/id> foaf:familyName": "Ratdolt",
			"<http://www.idref.fr/{key}/birth> bio:date": "1447",
			"<http://www.idref.fr/{key}/death> bio:date": "1528",
		};
		self.assertEqual(data, ("data.idref.fr", expected_data));

	def test_get_informations_from_idref_with_missing_elements(self):
		data = Actor.objects.get_informations_from_idref("027460959");
		expected_data = {
			"<http://www.idref.fr/{key}/id> foaf:name": "Alphonse X",
			"<http://www.idref.fr/{key}/birth> bio:date": "1221-11-26",
			"<http://www.idref.fr/{key}/death> bio:date": "1284-04-04",
		};
		self.assertEqual(data, ("data.idref.fr", expected_data));


	def test_get_actor_from_bnf(self):
		actor = Actor.objects.get_from_bnf("12148/cb178122927");
		self.assertIsNotNone(actor.id);
		self.assertEqual(actor.name, "Erhard Ratdolt");
		self.assertEqual(actor.given_name, "Erhard");
		self.assertEqual(actor.family_name, "Ratdolt");
		self.assertEqual(actor.birth_date, "1447");
		self.assertEqual(actor.death_date, "1528");
		self.assertEqual(actor.ark, "12148/cb178122927");
		sourced_properties = ["name", "family_name", "given_name", "birth_date", "death_date"];
		assert_data_sourced(actor, sourced_properties, "data.bnf.fr");

	def test_get_actor_from_bnf_with_missing_elements(self):
		actor = Actor.objects.get_from_bnf("12148/cb11997785j");
		self.assertIsNotNone(actor.id);
		self.assertEqual(actor.name, "");
		self.assertEqual(actor.given_name, "");
		self.assertEqual(actor.family_name, "Alphonse");
		self.assertEqual(actor.birth_date, "1221-11-26");
		self.assertEqual(actor.death_date, "1284-04-04");
		self.assertEqual(actor.ark, "12148/cb11997785j");
		sourced_properties = ["family_name", "birth_date", "death_date"];
		assert_data_sourced(actor, sourced_properties, "data.bnf.fr");

	def test_get_informations_from_bnf(self):
		data = Actor.objects.get_informations_from_bnf("12148/cb178122927");
		expected_data = {
			"<http://data.bnf.fr/ark:/{key}#about> foaf:name": "Erhard Ratdolt",
			"<http://data.bnf.fr/ark:/{key}#about> foaf:givenName": "Erhard",
			"<http://data.bnf.fr/ark:/{key}#about> foaf:familyName": "Ratdolt",
			"<http://data.bnf.fr/ark:/{key}#about> bio:birth": "1447",
			"<http://data.bnf.fr/ark:/{key}#about> bio:death": "1528",
		};
		self.assertEqual(data, ("data.bnf.fr", expected_data));

	def test_get_informations_from_bnf_with_missing_elements(self):
		data = Actor.objects.get_informations_from_bnf("12148/cb11997785j");
		expected_data = {
			"<http://data.bnf.fr/ark:/{key}#about> foaf:familyName": "Alphonse",
			"<http://data.bnf.fr/ark:/{key}#about> bio:birth": "1221-11-26",
			"<http://data.bnf.fr/ark:/{key}#about> bio:death": "1284-04-04",
		};
		self.assertEqual(data, ("data.bnf.fr", expected_data));


	def test_get_actor_from_viaf(self):
		actor = Actor.objects.get_from_viaf("17622641");
		self.assertIsNotNone(actor.id);
		self.assertEqual(actor.name, "Francisco García Ventanas");
		self.assertEqual(actor.given_name, "Franciscus");
		self.assertEqual(actor.family_name, "Ventanas");
		self.assertEqual(actor.birth_date, "1603");
		self.assertEqual(actor.death_date, "1678");
		sourced_properties = ["name", "family_name", "given_name", "birth_date", "death_date"];
		assert_data_sourced(actor, sourced_properties, "viaf.org");

	def test_get_actor_from_viaf_where_idref_is_defined(self):
		actor = Actor.objects.get_from_viaf("25398656");
		self.assertIsNotNone(actor.id);
		self.assertEqual(actor.name, "Erhard Ratdolt");
		self.assertEqual(actor.given_name, "Erhard");
		self.assertEqual(actor.family_name, "Ratdolt");
		self.assertEqual(actor.birth_date, "1447");
		self.assertEqual(actor.death_date, "1528");
		self.assertEqual(actor.viaf_id, "25398656");
		self.assertEqual(actor.ppn, "095008152");
		sourced_properties = ["name", "family_name", "given_name", "birth_date", "death_date"];
		assert_data_sourced(actor, sourced_properties, "data.idref.fr");
		assert_data_sourced(actor, ["ppn"], "viaf.org");

	def test_get_actor_from_viaf_where_bnf_is_defined(self):
		actor = Actor.objects.get_from_viaf("312749706");
		self.assertIsNotNone(actor.id);
		self.assertEqual(actor.name, "Johann Lucilius Santritter");
		self.assertEqual(actor.given_name, "Johann Lucilius");
		self.assertEqual(actor.family_name, "Santritter");
		self.assertEqual(actor.birth_date, "14..");
		self.assertEqual(actor.death_date, "15..");
		self.assertEqual(actor.viaf_id, "312749706");
		self.assertEqual(actor.ark, "12148/cb131646029");
		sourced_properties = ["name", "family_name", "given_name", "birth_date", "death_date"];
		assert_data_sourced(actor, sourced_properties, "data.bnf.fr");
		assert_data_sourced(actor, ["ark"], "viaf.org");

	def test_get_informations_from_viaf(self):
		data = Actor.objects.get_informations_from_viaf("17622641");
		expected_data = {
			"<http://viaf.org/viaf/{key}> sdo:name": "Francisco García Ventanas",
			"<http://viaf.org/viaf/{key}> sdo:givenName": "Franciscus",
			"<http://viaf.org/viaf/{key}> sdo:familyName": "Ventanas",
			"<http://viaf.org/viaf/{key}> sdo:birthDate": "1603",
			"<http://viaf.org/viaf/{key}> sdo:deathDate": "1678",
		};
		self.assertEqual(data, ("viaf.org", expected_data));


	def test_actor_to_string(self):
		actor = Actor(given_name="John", family_name="Doe");
		actor.birth_date="1234-06-15";
		actor.death_date="1235";
		self.assertEqual(actor.__str__(), "John Doe");
		self.assertEqual(actor.dates, "1234--1235");
		actor.name = "Gérard Bidule de la Motte";
		self.assertEqual(actor.__str__(), "Gérard Bidule de la Motte");
		self.assertEqual(actor.dates, "1234--1235");

	def test_actor_to_string_with_only_death_date(self):
		actor = Actor(given_name="John", family_name="Doe");
		actor.death_date="1235";
		self.assertEqual(actor.dates, "d.1235");

	def test_actor_to_string_with_only_birt_date(self):
		actor = Actor(given_name="John");
		actor.birth_date="1234";
		self.assertEqual(actor.dates, "b.1234");

	def test_actor_to_string_without_dates(self):
		actor = Actor(name="John Doe");
		self.assertEqual(actor.__str__(), "John Doe");


	def test_get_actor_from_name(self):
		actor = Actor.objects.get_from_name("Alphonso de Cordoba");
		self.assertIsNotNone(actor.id);
		self.assertEqual(actor.viaf_id, "280836932");
		self.assertEqual(actor.ppn, "153007869");
		assert_data_sourced(actor, ["viaf_id"], "viaf.org");

	@skip("One way to fix this would be to search for results in viaf after the first page, but this leads to the failure of test_actor_without_viaf_cluster")
	def test_get_actor_from_name_bis(self):
		actor = Actor.objects.get_from_name("Johannes Hanau");
		self.assertEqual(actor.viaf_id, "301575763");

	def test_get_multiple_actors_from_name(self):
		(montanus, neuberus) = Actor.objects.get_from_names("Johannis Montanus and Ulrichus Neuberus");
		self.assertIsNotNone(montanus.id);
		self.assertIsNotNone(neuberus.id);
		self.assertEqual(montanus.viaf_id, "266366440");
		self.assertEqual(neuberus.viaf_id, "23044512");

	def test_get_multiple_actors_from_names_bis(self):
		(montanus, neuberus) = Actor.objects.get_from_names("Johannes Montanus and Ulricus Neuber");
		self.assertEqual(montanus.viaf_id, "266366440");
		self.assertEqual(neuberus.viaf_id, "454152636069920051533");

	def test_get_actor_from_alternative_names(self):
		regiomontanus_id = "17261967";
		regiomontanus_names = ["Regiomontanus", "Johannes Müller", "Johannes Regiomontanus", "Johann Müller of Königsberg", "Johann Müller of Königsberg (Regiomontanus)"];
		for name in regiomontanus_names:
			(actor) = Actor.objects.get_from_name(name);
			self.assertIsNotNone(actor.id);
			self.assertEqual(actor.viaf_id, regiomontanus_id);

	def test_get_same_actor_for_different_orthographs(self):
		actor1 = Actor.objects.get_from_name("Alfonse X de Castille");
		actor2 = Actor.objects.get_from_name("Alphonsus, Rex Castellae");
		self.assertEqual(actor1.viaf_id, actor2.viaf_id);
		self.assertEqual(actor1.viaf_id, "66476694");

	def test_equivalent_actors(self):
		actor1 = Actor.objects.get_from_name("Ulricus Neuber");
		actor2 = Actor.objects.get_from_name("Ulricus Neuberus");
		actor1.same_as.add(actor2);
		self.assertTrue(actor1.same_as.contains(actor2));
		self.assertTrue(actor2.same_as.contains(actor1));
		self.assertEqual(actor1.same_as.count(), 1);
		self.assertEqual(actor2.same_as.count(), 1);
		self.assertEqual(actor1.same_as.first(), actor2);
		self.assertEqual(actor2.same_as.first(), actor1);

	def test_search_for_invalid_name(self):
		actor = Actor.objects.get_from_name("Nobody has that name");
		self.assertIsNone(actor.viaf_id);

	def test_actor_without_viaf_cluster(self):
		actor = Actor.objects.get_from_name("Officina Regia");
		self.assertIsNone(actor.viaf_id);


class PlaceTest(TestCase):
	def test_get_place_from_geonames(self):
		place = Place.objects.get_from_geonames(2988507);
		self.assertEqual(place.name, "Paris")
		self.assertEqual(place.latitude, 48.85341)
		self.assertEqual(place.longitude, 2.3488)
		sourced_properties = ["name", "latitude", "longitude"];
		assert_data_sourced(place, sourced_properties, "geonames.org");

	def test_get_informations_from_geonames(self):
		data = Place.objects.get_data_from_geonames(2988507);
		expected_data = {
			"toponymName": "Paris",
			"lat": 48.85341,
			"long": 2.3488,
		};
		self.assertEqual(data, ("geonames.org", expected_data));


