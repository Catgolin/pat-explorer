from django.contrib import admin

from .models import Actor, Place;

admin.site.register(Actor);
admin.site.register(Place);
