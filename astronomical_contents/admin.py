from django.contrib import admin

from .models import TableType, AstronomicalObject;

@admin.register(TableType)
class TableTypeAdmin(admin.ModelAdmin):
	save_as = True;
	list_display = ('__str__', 'astronomical_object');

@admin.register(AstronomicalObject)
class AstronomicalObjectAdmin(admin.ModelAdmin):
	pass;
