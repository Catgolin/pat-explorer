import asyncio;
import flatdict;
import logging;
import typing;

from django.db.models import Manager;

from commons import network, hydrator;

from . import models;

logger = logging.getLogger(__name__);

class TableTypeManager(Manager):

	def all(self, *args, **kwargs):
		return super().all(*args, **kwargs).order_by("astronomical_object__id", "name");

	def get_from_dishas(self, dishas_id: int, data_type: str = "table_type"):
		if not dishas_id:
			return None;

		tuple_data = self.get_informations_from_dishas(dishas_id, data_type);
		if not tuple_data:
			return None;
		source, data = tuple_data;

		if "source" in data.keys():
			del data["source"];

		table_type_id = dishas_id if "table_type" == data_type else int(data["table_type.id"]);

		table_type = self.get_table_type_from_dishas_data(table_type_id, source, **data);

		if "edited_text" == data_type:
			table_type.table_edition = dishas_id;
			table_type.save();

		return table_type;

	def get_table_type_from_dishas_data(self, dishas_id: int, source: str = "dishas.obspm.fr", **data):
		table_type = self.get_or_create(dishas_id = dishas_id)[0];
		hydrator.update_model_from_source_data(table_type, source, **data);
		astronomical_object = models.AstronomicalObject.objects.get_from_dishas_data(source = source, **data);
		if astronomical_object:
			table_type.astronomical_object = astronomical_object;
			hydrator.update_source_of_model(table_type, source, "astronomical_object");
		return table_type;

	def get_informations_from_dishas(self, dishas_id: int, data_type: str = "table_type") -> typing.Optional[tuple[str, dict]]:
		if not dishas_id:
			return None;

		url = "https://dishas.obspm.fr/elastic-query";
		params = {
			"index": data_type,
			"query": '{"match":{"id":"%s"}}' % dishas_id,
		};
		data = asyncio.run(network.get_json(url, params, field="hits"));
		if len(data["hits"]) < 1:
			logger.error(f"No data from https://dishas.obspm.fr for {data_type} with id '{dishas_id}'");
			return None;
		raw_data = data["hits"][0]["_source"];
		flatten_data = flatdict.FlatDict(raw_data, delimiter=".");
		return ("dishas.obspm.fr", flatten_data);


class AstronomicalObjectManager(Manager):
	def get_from_dishas_data(self, source: str = "dishas.obspm.fr", **data):
		possible_keys = [
			"astronomical_object.id",
			"table_type.astronomical_object.id",
			"id",
		];
		if set(possible_keys).isdisjoint(data.keys()):
			logger.warning(f"There is no data to initialize the astronomical object in {data.keys()}");
			return None;
		dishas_id = None;
		for key in possible_keys:
			if key in data.keys():
				dishas_id = int(data[key]);
				break;
		if not dishas_id:
			return None;

		astronomical_object = self.get_or_create(dishas_id = dishas_id)[0];
		hydrator.update_model_from_source_data(astronomical_object, source, **data);
		return astronomical_object;
