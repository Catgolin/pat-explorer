from django.apps import AppConfig


class AstronomicalContentsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'astronomical_contents'
