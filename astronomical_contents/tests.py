from unittest import skip;
from django.test import TestCase;

from commons.tests import assert_data_sourced;

from .models import TableType;

class TableTypeTest(TestCase):
	def test_get_from_dishas(self):
		table_type = TableType.objects.get_from_dishas(14)
		self.assertEqual(table_type.name, "mean motion apogees and stars/precessions")
		self.assertEqual(table_type.python_name, "mean_motion_apogees_and_stars_precessions")
		self.assertEqual(table_type.definition, "mean motion used to compute all or part of the eight motion depending on the theory adopted")
		self.assertEqual(table_type.dishas_name, "Eighth sphere | Mean motion apogees and stars/precessions")
		sourced_properties = ["name", "python_name", "definition", "dishas_name", "astronomical_object"];
		assert_data_sourced(table_type, sourced_properties, "dishas.obspm.fr");
		sphere = table_type.astronomical_object
		self.assertEqual(sphere.name, "eighth sphere")
		self.assertEqual(sphere.dishas_id, 9)
		self.assertEqual(sphere.definition, "Imaginary sphere carrying all the constellations of the fixed stars, observed to move with respect to the referential of the ecliptic circle, in particular to the position of the Sun at the equinoxes.")
		sourced_properties = ["name", "definition"];
		assert_data_sourced(sphere, sourced_properties, "dishas.obspm.fr");

	def test_get_from_dishas_edition(self):
		table_type = TableType.objects.get_from_dishas(149, data_type="edited_text");
		self.assertEqual(table_type.dishas_id, 4);
		self.assertEqual(table_type.table_edition, 149);
		self.assertEqual(table_type.name, "equation of the Sun");
		self.assertEqual(table_type.python_name, "equ_of_the_sun");
		self.assertEqual(table_type.astronomical_object.dishas_id, 1);
		self.assertEqual(table_type.astronomical_object.name, "sun");
		self.assertEqual(table_type.astronomical_object.definition, "The most brilliant of the objects in the sky, appears to change its place with respect to the fixed stars about 1° per day.");

	def test_get_table_content(self):
		table_type = TableType.objects.get_from_dishas(149, data_type = "edited_text");
		content = table_type.get_exemple_content();

		# Is there a column for arguments
		self.assertTrue("args" in content.keys(), content.keys());
		self.assertTrue("argument1" in content["args"].keys(), content["args"].keys());
		arguments = content["args"]["argument1"];

		# Is there a column for entries
		self.assertTrue("entry" in content.keys(), content.keys());
		self.assertEqual(len(arguments), len(content["entry"]));
		self.assertEqual(len(arguments), 360);
		self.assertTrue("data" in content.keys(), content.keys());
		self.assertEqual(len(content["data"]), len(arguments));

		# Are there informations about the template
		self.assertTrue("template" in content.keys(), content.keys());
		template = content["template"];
		self.assertTrue("args" in template.keys(), template.keys());
		self.assertTrue("entries" in template.keys(), template.keys());
		self.assertTrue("name" in template["args"][0].keys(), template["args"][0].keys());
		self.assertTrue("name" in template["entries"][0].keys(), template["entries"][0].keys());
		self.assertTrue("ncells" in template["args"][0].keys(), template["args"][0].keys());
		self.assertTrue("ncells" in template["entries"][0].keys(), template["entries"][0].keys());

		nb_of_columns = template["args"][0]["ncells"] + template["entries"][0]["ncells"];

		# Is there a big array with all of the data
		for row in content["data"]:
			with self.subTest(row=row):
				self.assertEqual(len(row), nb_of_columns);

	def test_print_table_content(self):
		table_type = TableType.objects.get_from_dishas(149, data_type = "edited_text");
		response = self.client.get(f"/astronomy/type/{table_type.id}");
		self.assertEqual(response.status_code, 200);

