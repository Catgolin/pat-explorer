from django.shortcuts import render;
from django.views import generic;

from .models import AstronomicalObject, TableType;

def types_index(request):
	context = {
		"objects_list": AstronomicalObject.objects.all(),
		"alone_types": TableType.objects.filter(astronomical_object__isnull=True),
	};
	return render(request, "astronomical_contents/tabletype_list.html", context);

class TypeDetailView(generic.DetailView):
	model = TableType;
