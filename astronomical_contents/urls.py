from django.urls import path;

from . import views;

app_name = "astronomical_contents";

urlpatterns = [
	path('types', views.types_index, name="types"),
	path('type/<int:pk>', views.TypeDetailView.as_view(), name="type"),
];
