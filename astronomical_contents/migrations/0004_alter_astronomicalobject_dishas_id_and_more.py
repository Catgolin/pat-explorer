# Generated by Django 4.2 on 2023-05-04 12:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('astronomical_contents', '0003_rename_table_edtion_tabletype_table_edition'),
    ]

    operations = [
        migrations.AlterField(
            model_name='astronomicalobject',
            name='dishas_id',
            field=models.IntegerField(blank=True, help_text="Identifier of the astronomical object in the disahs.obspm.fr's database", null=True, unique=True, verbose_name="Dishas' identifier"),
        ),
        migrations.AlterField(
            model_name='tabletype',
            name='astronomical_object',
            field=models.ForeignKey(blank=True, help_text='Object related to the content of this tables', null=True, on_delete=django.db.models.deletion.CASCADE, to='astronomical_contents.astronomicalobject', verbose_name='object'),
        ),
        migrations.AlterField(
            model_name='tabletype',
            name='chabas_description_page_end',
            field=models.IntegerField(blank=True, help_text="Last page of José Chabás' description in Computational Astronomy in the Middle Ages (2019)", null=True, verbose_name="Chabás' description end"),
        ),
        migrations.AlterField(
            model_name='tabletype',
            name='chabas_description_page_start',
            field=models.IntegerField(blank=True, help_text="First page of José Chabás' description in Computational Astronomy in the Middle Ages (2019)", null=True, verbose_name="Chabás' description start"),
        ),
        migrations.AlterField(
            model_name='tabletype',
            name='chabas_number',
            field=models.IntegerField(blank=True, help_text='Number of this type of table in José Chabás, Computational Astronomy in the Middle Ages (2019)', null=True, verbose_name="Chabas' number"),
        ),
        migrations.AlterField(
            model_name='tabletype',
            name='dishas_id',
            field=models.IntegerField(blank=True, help_text="Identifier of the table type in the disahs.obspm.fr's database", null=True, verbose_name="Dishas' identifier"),
        ),
        migrations.AlterField(
            model_name='tabletype',
            name='table_edition',
            field=models.IntegerField(blank=True, help_text='Identifier of a table edition of this table type in the Alfonsine tradition on dishas.obspm.fr', null=True, verbose_name="Dishas' edition identifier"),
        ),
    ]
