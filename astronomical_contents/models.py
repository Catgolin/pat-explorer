import asyncio;
import logging;
from django.db import models
from django.utils.translation import gettext_lazy as _;

from commons import network;

from . import managers;

logger = logging.getLogger(__name__);

class AstronomicalObject(models.Model):
	""" The subject of the tables """
	@staticmethod
	def ontology():
		return {
			"keys": {
				"dishas_id": "dishas.obspm.fr",
			},
			"sources": {
				"dishas.obspm.fr": {
					"id": "dishas_id",
					"object_name": "name",
					"object_definition": "definition",

					"astronomical_object.id": "dishas_id",
					"astronomical_object.object_name": "name",
					"astronomical_object.object_definition": "definition",

					"table_type.astronomical_object.id": "dishas_id",
					"table_type.astronomical_object.object_name": "name",
					"table_type.astronomical_object.object_definition": "definition",
				}
			},
		};

	objects = managers.AstronomicalObjectManager();

	class Meta:
		verbose_name = _("astronomical object");
		verbose_name_plural = _("astronomical objects");

	#   ===   Database Links   ===
	dishas_id = models.IntegerField(
		_("Dishas' identifier"),
		null=True, blank=True,
		unique=True,
		help_text = _("Identifier of the astronomical object in the disahs.obspm.fr's database"),
	);

	#   ===   Object description   ===
	name = models.CharField(
		_("name"),
		max_length = 200,
		blank=True,
		help_text = _("Name of this object"),
	);
	definition = models.TextField(
		_("definition"),
		blank=True,
		help_text = _("Definition of this object"),
	);
	color = models.CharField(
		_("color"),
		max_length = 100,
		blank=True,
		help_text = _("color of this object"),
	);

	#   ===   Sources   ===
	data_sources = models.TextField(
		_("sources of the informations"),
		blank=True,
		help_text = _("Source of the informations stored about this object"),
	);

	def __str__(self):
		return self.name

class TableType(models.Model):
	@staticmethod
	def ontology():
		return {
			"keys": {},
			"sources": {
				"dishas.obspm.fr": {
					"table_type.id": "dishas_id",
					"table_type.table_type_name": "name",
					"table_type_name": "name",
					"table_type.kibana_name": "dishas_name",
					"kibana_name": "dishas_name",
					"table_type.python_name": "python_name",
					"python_name": "python_name",
					"table_type.astro_definition": "definition",
					"astro_definition": "definition",
				},
			},
		};

	objects = managers.TableTypeManager();

	class Meta:
		verbose_name = _("table type");
		verbose_name_plural = _("table types");


	#   ===   Database links   ===
	dishas_id = models.IntegerField(
		_("Dishas' identifier"),
		null=True, blank=True,
		help_text = _("Identifier of the table type in the disahs.obspm.fr's database"),
	);
	table_edition = models.IntegerField(
		_("Dishas' edition identifier"),
		null = True, blank=True,
		help_text = _("Identifier of a table edition of this table type in the Alfonsine tradition on dishas.obspm.fr"),
	);
	python_name = models.CharField(
		_("Kanon's name"),
		max_length=200,
		blank=True,
		help_text = _("Name of this table type in the Kanon API"),
	);

	#   ===   Identification of the table type   ===
	name = models.CharField(
		_("name"),
		max_length = 200,
		blank=True,
		help_text = _("Human-readable name of this type of table"),
	);
	dishas_name = models.CharField(
		_("Dishas' name"),
		max_length=200,
		blank=True,
		help_text = _("Complete name of this table in Dishas"),
	);

	#   ===   Tables informations   ===
	astronomical_object = models.ForeignKey(
		AstronomicalObject,
		verbose_name = _("object"),
		on_delete=models.CASCADE,
		null=True, blank=True,
		help_text = _("Object related to the content of this tables"),
	);
	definition = models.TextField(
		_("definition"),
		blank=True,
		help_text = _("Definition of the content of this type of tables"),
	);

	#   ===   Bibliographic reference   ===
	chabas_name = models.CharField(
		_("name from José Chábas' publication"),
		max_length=200,
		blank=True,
		help_text = _("Name given to this type of tables in José Chábas, Computational Astronomy in the Middle Ages (2019)"),
	);
	chabas_number = models.IntegerField(
		_("Chabas' number"),
		null=True, blank=True,
		help_text =_("Number of this type of table in José Chabás, Computational Astronomy in the Middle Ages (2019)"),
	);
	chabas_description_page_start = models.IntegerField(
		_("Chabás' description start"),
		null=True, blank=True,
		help_text = _("First page of José Chabás' description in Computational Astronomy in the Middle Ages (2019)"),
	);
	chabas_description_page_end = models.IntegerField(
		_("Chabás' description end"),
		null=True, blank=True,
		help_text = _("Last page of José Chabás' description in Computational Astronomy in the Middle Ages (2019)"),
	);

	#   ===   Sources   ===
	data_sources = models.TextField(
		_("sources of the informations"),
		blank=True,
		help_text = _("Source of the informations stored about this table type"),
	);

	def get_name(self):
		if self.name:
			return self.name;
		if self.dishas_name:
			return self.dishas_name;
		if self.chabas_name:
			return self.chabas_name;
		return _("No name");

	def __str__(self):
		if self.name:
			return f"{self.astronomical_object} | {self.name}";
		if self.dishas_name:
			return self.dishas_name;
		if self.chabas_name:
			return f"{self.astronomical_object} | {self.chabas_name}";
		return _("No name");

	def get_exemple_content(self) -> dict:
		if not self.table_edition:
			return {};

		url = "https://dishas.obspm.fr/elastic-query";
		params = {
			"index": "edited_text",
			"query": '{"match":{"id":"%s"}}' % self.table_edition,
		};
		raw_data = asyncio.run(network.get_json(url, params, field="hits"));
		if None == raw_data or len(raw_data["hits"]) < 1:
			logger.error(f"No data from https://dishas.obspm.fr for table edition with id '{self.table_edition}'");
			return {};

		data = raw_data["hits"][0]["_source"]["table_contents"][0]["value_original"];

		data["editor"] = raw_data["hits"][0]["_source"]["intellectual_authors"];

		numbers = [];
		for i, args in enumerate(data["args"]["argument1"]):
			row = args["value"];
			if "argument2" in data["args"].keys():
				row += data["args"]["argument2"][i]["value"];
			row += data["entry"][i]["value"];
			numbers.append(row);

		data["data"] = numbers;

		return data;
