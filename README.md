# Printed Alfonsine Tables Explorer

This project is meant to help me analyze the content of the printed editions
of the Parisian Alfonsine Tables produced in Latin between 1483 and 1641
as part of my Master's thesis.

It is primarely based upon the work and data of the [Dishas project](https://dishas.obspm.fr),
but the goal is to connect data from diverse origins:
- From [Abes's IdRef database](https://data.idref.fr)
  and from the database of the [Bibliothèque Nationale de France](https://data.bnf.fr),
  both federated with other sources in the [OCLC's viaf.org database](https://viaf.org);
- From the [Incunabala Short Title Catalogue](https://data.cerl.org/istc)
  of the Consortium of European Research Libraries;
- Some data concerning `Place`s are taken from the [geonames database](https://geonames.org);
All these databases allow the use of their datas, either under the free CC-BY licence,
or under the public-domain Etalab licence.

## Project structure

### `scenery`

The `scenery` module defines `Actor`s and `Place`s

In terms of dependencies, the `scenery` application could have be coupled with the `works` application,
but I thought it could be a good idea to keep the `scenery` appart
because one could try to add more complexity to this module.
In particular, it could be interesting to see where the `Actor`s were at some particular dates,
which ones were associated together in a printing workshop.
It could also be interesting to add informations about the political context
in the places where works were printed.
These informations are currently out of the scope of this application,
but the decoupling of the `scenery` and the `works` leaves room for improvement.

### `astronomical_content`

The `astronomical_content` module defines `TableType`s
and the `AstronomicalObject`s they are related to.

The `astronomical_content`'s application
allows me to analyse the astronomical content of the `Actor`s's `works`
from a mathematical perspective.
To this end, the goal of this module is to be fed with informations
extracted from the `source_materials`, and to use the `Kanon` library.

### `works`

The `works` module describes `Title`s and `Corpus`es created by `Actor`s.

- *dependencies: `scenery`*

This module describes informations about the printed editions I study,
and some relationship between them.
In the context of the [Dishas project](https://dishas.obspm.fr/glossary),
a *Work* is defined as a distinct intellectual creation.
This definition takes inspiration
from the International Federation of Library Associations and Institutions's
[Fonctional Requirements for Bibliographic Records](https://www.ifla.org/files/assets/cataloguing/frbr/frbr_2008.pdf).
From there, we could distinguish between four kinds of entities:
*works*, *expressions*, *manifestaions* and *items*.

About the first two entities, *works* would then correspond to the Dishas' definition,
and the *expressions* would be the canons and the specific versions
of the `TableType`s contained in each individual *Works*.
However, on the begining of my investigation about the printed editions
of the Parisian Alfonsine Tables,
it would be hard to get a clear and usefull definition of what a *Work* shoud be.
Indeed, 15/16/17th-century printers had varying claims
about the significance of their contributions in the production of a book,
and it is one objective of my research to understand
what could be considered as a **distinct** intellectual "creation" in this context.
This is where the notion of `Corpus` comes in:
the goal is to have broad categories of intellectual objects
that can be brought together in order to identify wich ones are of interest for my study,
and which one are less directly interesting.
But in order to go further into the description of my notion of `Corpus`,
I need first to talk about the two other entities
defined by the International Federation of Library Associations and Institutions's requirements.

The next two entities deal about *manifestations* and *items*.
As for the Fonctional Requirements for Bibliographic Records's framework,
a *manifestation* is "the physical embodiment of an *expression* of a *work*".
In the context of printed books, this notion of *manifestation* would correspond
to one particular edition, printed in one or (usually) many copies.
This is what the Consortium of European Research Libraries and some other entities
have formalized under the concept of `Title`.
On the other hand, an *item* is one individual exemplification of a `Title`:
this exemplification could be physical or digital,
and in both cases I chose to describe it as a `Witness`, in the `source_materials` module.

The assumption is that each `Title` has some kind of a `name` allowing to identify it.
In practice, it is not always obvious to understand what this `name` should be,
because an early-printed `Title` doesn't always have a clear *title* on it's cover or first page,
but instead a lot of different kinds of texts to describe it in various manners.
This is in part the reason why I try to align my datas with the informations
provided with international databases,
in particular the *Incunabala Short Titles Catalogue*:
they provide a stable `name` for the `Title`s I consider.
In some respect, the choice of this `name` is quite an arbitrary decision
from a 20/21th-century observer, but it still provides some informations
about what is the *genre* of the *manifestation*.
Hence, it is based on the `name` that I first affect each `Title` to one
or many given `Corpus`es.

- *Note:*
  The difference between a *manifestation* and an *item*,
  or between a `Title` and a `Witness` (or, in his terms, between a *book* and a *book-copy*)
  was notably made clear in the context of early-printed books by Joseph Dane
  in *The Myth of Print Culture. Essays on Evidence, Textuality and bibliographical Method*,
  published in 2003 by the University of Toronto Press.

### `source_materials`

The `source_materials` module describes indivial `Witness`es of a `Title`,
and their holding institutions.

- *dependencies: `scenery` and `works`*

Based on the distinctions between *works*, *expressions*,
*manifestations* and *items* previously described,
a `Witness` is one particular *item* of a `Title` *manifestation*.
Hence, a `Witness` is a copy of a book, which is stored in a `Library`,
or a digital reproduction of such "copy of a book".

### `edition`

The `edition` module describes the astronomical content of a `Witness`
in terms of `Table`s implementing a `TableType`.

- *dependencies: `source_materials` and `astronomical_content`*

The `edition` module helps me analyze the `astronomical_content`
described in the `Witness`es of the `Title`s of my `Corpus`.
In particular, it is meant to identify
the *table of contents* of each `Witness`,
in order to identify how the `Table`s instanciating
the same `TableType` evolved between different `Title`s.

Even though my goal through this application
is not to produce editions of the alfonsine tables,
I kept the *edition* denomination for the sake of consistency
with the [Dishas project's designation](https://dishas.obspm.fr/glossary),
where the focus was set on the fact that the tables presented were eddited by historians.

### `common`

The `common` module contains methods that are usefull in every other module of the application,
mainly for aligning entities with external databases.

## How to use

### Installation

1. Make sure to have all the dependencies installed
2. Create a `.env` file with a `geonames_key` containing an identifier for the [geonames.org's API](https://geonames.org)
   and a `SECRET_KEY` for [Django's session management](https://docs.djangoproject.com/en/4.2/ref/settings/#secret-key).
   - Add a `DJANGO_SUPERUSER_PASSWORD` to the `.env` file and you can use the `make repopulate` command to reset the database.

### Usage

To navigate through the web interface:
3. Run `python manage.py runserver` to launch the local server, and connect to the indicated URL
   - The `/sources` route allows to see the list of all `Witness`es, organized by `Corpus

To generate graphs:
3. Run `python manage.py shell` and import the appropriate views
   - The `works.views.DataGraph` class generates a graph of all the `Title`s.
     - Run the `show_complete_graph()` method to show the graph in a `pyplot` frame.
   - The `works.views.GeoData` class generates maps of the `Title`s.
     - Run the `show_titles_places()` method to show a static map in a `pyplot` frame.
     - Run the `animte_titles_places()` method to show an animated map in a `pyplot` frame,
       where the `Titles` appear by order of their publication.
   - The `works.views.DataChart` class generates charts.
     - Run the `show_corpuses_pie()` method to see a pie-chart
       showing the number of `Title`es in each `Corpus` in a `pyplot` frame.


### Testing

- The `make test` command runs almost every test with two parallel runners,
  testing against real APIs.
  This takes around two to three minutes.
- The `make complete-test` command runs even the longest tests,
  loading complete sets of data (this takes around ten minutes).

Both these test commands need an Internet connexion, and depend greatly on the speed
of this connexion.
It might be a good idea to update these tests some day to have fake API results
instead of relying on a real internet connection, but this is how it is for now.

## Dependencies:

Here are the dependencies used in this project,
along with the indication of the versions I used while testing it on my computers.

- `CPython` (version 3.9.2)
  - **Source code** (and issue tracking): https://github.com/python/cpython
  - **Licence:** https://github.com/python/cpython/blob/main/LICENSE
  - **Documentation:** https://docs.python.org/3/

- `django` (version 4.2.0)
  - **Source code** (and issue tracking): https://github.com/django/django
  - **Licence:** BSD 3-Clause
  - **Documentation:** https://docs.djangoproject.com/en/4.2/

### Dependencies helping connexion to external databases
- `heurist` (version 1.0.0):
  - **Source code** (and issue tracking): https://gitlab.obspm.fr/salbouy/heurist-data
  - **Licence:** None
  - **Documentation:** None
  - **Usage:** Gets informations from the ALFA survey database.
- `geocoder` `pip install geocoder` (version 1.38.1)
  - **Source code** (and issue tracking): https://github.com/DenisCarriere/geocoder
  - **Licence:** MIT
  - **Documentation:** https://geocoder.readthedocs.io/
  - **Usage:** Extract informations about places from the `geonames.org`'s API.
    The username used to connect to the API has to be defined in the `.env` file.

### Dependencies helping to get and parse structured data
- `rdflib`: `pip install rdflib` (version 6.2.0)
  - **Source code** (and issue tracking): https://github.com/RDFLib/rdflib
  - **Licence:** BSD 3-Clause
  - **Documentation:** https://rdflib.readthedocs.io/en/stable/
  - **Usage:** Used to execute Sparql queries and to parse RDF data.
- `json` (default CPython library - version 2.0.9)
  - **Source code** (and issue tracking): https://github.com/python/cpython/tree/3.11/Lib/json
  - **Documentation:** https://docs.python.org/3/library/json.html
  - **Usage:** Parses raw JSON data returned by the `requests` package into Python dictionaries and lists.
- `yaml`: `pip install pyyaml` (version 5.3.1)
  - **Source code** (and issue tracking): https://github.com/yaml/pyyaml
  - **Licence:** MIT
  - **Documentation:** https://pyyaml.org/wiki/PyYAMLDocumentation
  - **Usage:** Parses raw YAML data returned by the `requests` package into Python dictionary and lists.

### Dependencies helping to align and analyze data
- `fuzzywuzzy`: `pip install fuzzywuzzy` (version 0.18.0)
  - **Source code** (and issue tracking): https://github.com/seatgeek/fuzzywuzzy
  - **Licence:** GNU-GPL v2.0
  - **Documentation:** None (project has been moved to `thefuzz`, it could be a good idea to switch to the new version)
  - **Usage:** Detects closeness of strings that are not strictly equal.
    Especially useful while searching names in viaf clusters.
- `python-Levenshtein`: `pip install python-Levenshtein`
  - **Source code** (and issue tracking): https://github.com/maxbachmann/python-Levenshtein
  - **Licence:** GNU-GPL v2.0
  - **Documentation:** https://maxbachmann.github.io/Levenshtein/
  - **Usage:** Dependency of `fuzzywuzzy`
  - Note: Package has been moved to https://github.com/maxbachmann/levenshtein but both are maintained the same way.
- `re`: (default CPython library - version 2.2.1)
  - **Source code** (and issue tracking): https://github.com/python/cpython/tree/3.11/Lib/re/
  - **Documentation:** https://docs.python.org/3/library/re.html
  - **Usage:** Runs regular expressions.
- `urllib` (default CPython library)
  - **Source code** (and issue tracking): https://github.com/python/cpython/tree/3.11/Lib/urllib
  - **Documentation:** https://docs.python.org/3/library/urllib.html
  - **Usage:** Helps to detect paterns in a URL (mainly the `parse` module).
- `flatdict`: `pip install flatdict` (version 4.0.1)
  - **Source code** (and issue tracking): https://github.com/gmr/flatdict
  - **Licence:** BSD 3-Clause
  - **Documentation:** https://flatdict.readthedocs.io/en/stable/
  - **Usage:** Converts a dictionnary containing dictionnaries into one single flat dictionary.

### Dependencies for data visualization
- `networkx`: `pip install networkx` (version 2.8.8)
  - **Source code** and issue tracking: https://github.com/networkx/networkx
  - **Licence:** BSD 3-Clause
  - **Documentation:** https://networkx.org/documentation/stable/
  - **Usage:** Graph visualization
- `matplotlib`: `pip install matplotlib` (version 3.6.2)
  - **Source code** and issue tracking: https://github.com/matplotlib/matplotlib
  - **Licence:** https://github.com/matplotlib/matplotlib/blob/main/LICENSE/LICENSE
  - **Documentation:** https://matplotlib.org/stable/index.html
  - **Usage:** Drawing graphs, charts and pictures
- `cartopy`
  - **Source code** and issue tracking: https://github.com/SciTools/cartopy
  - **Licence:** GNU LGPL
  - **Documentation:** https://scitools.org.uk/cartopy/docs/latest/
  - **Usage:** Drawing maps
  - **Notes:** Imports data from [Natural Earth](https://www.naturalearthdata.com) made available in Public Domain.
### System dependencies
- `dotenv`: `pip install python-dotenv`
  - **Source code** (and issue tracking): https://github.com/theskumar/python-dotenv
  - **Licence:** BSD 3-clause
  - **Documentation:** https://saurabh-kumar.com/python-dotenv/
  - **Usage:** Extract environment variables from `.env` files.
- `os` (default CPython library)
  - **Source code** (and issue tracking): https://github.com/python/cpython/blob/3.11/Lib/os.py
  - **Documentation:** https://docs.python.org/3/library/os.html
  - **Usage:** Extracts environment variables from `.env` files with `dotenv` and manages logger handler.
- `requests`: `pip install requests` (version 2.25.1)
  - **Source code** (and issue tracking): https://github.com/psf/requests
  - **Licence:** Apache 2.0
  - **Documentation:** https://requests.readthedocs.io/en/latest/
  - **Usage:** Sends HTTP requests and gets results.
- `logging` (default CPython library - version 0.5.1.2)
  - **Source code** (and issue tracking): https://github.com/python/cpython/blob/3.11/Lib/logging/
  - **Documentation:** https://docs.python.org/3/library/logging.html
    (In the context of Django: https://docs.djangoproject.com/en/4.2/topics/logging/ )
  - **Usage:** Passes log information to appropriate handlers controlled by Django configuration variables.
- `asyncio` (default CPython library)
  - **Source code** (and issue tracking): https://github.com/python/cpython/tree/3.11/Lib/asyncio/
  - **Documentation:** https://docs.python.org/3/library/asyncio.html
  - **Usage:** Runs concurrent operations, mainly while waiting on requests to external databases.
- `asgiref` (comes with `django` - version 3.6.0)
  - **Source code** (and issue tracking): https://github.com/django/asgiref/
  - **Licence:** BSD 3-Clause
  - **Documentation:** https://asgi.readthedocs.io/en/latest/
  - **Usage:** Synchronizing asynchronous operations, mainly to avoir security issues with the database.
- `csv` (default CPython library - version 1.0)
  - **Source code** (and issue tracking): https://github.com/python/cpython/blob/3.11/Lib/csv.py
  - **Documentation:** https://docs.python.org/3/library/csv.html
  - **Usage:** Load data from local CSV files

### Test dependencies:
- `unittest` (default CPython library)
  - **Source code** (and issue tracking): https://github.com/python/cpython/blob/3.11/Lib/unittest
  - **Documentation:** https://docs.python.org/3/library/unittest.html
  - **Usage:** Runs unit tests.

I also use the sorttable library from Stuart Langridge to display tables in JavaScript:
https://www.kryogenix.org/code/browser/sorttable/
