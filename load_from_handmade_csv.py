import typing;
from source_materials.models import Witness;
from works.models import Title;
from scenery.models import Actor, Place;
from astronomical_contents.models import TableType;
from commons import hydrator;
import csv
import logging

logger = logging.getLogger(__name__);

CSV_TO_KEY = {
	"title.dishas_id": [
		"dishas_id",
	],
	"title.print_date": [
		"Année",
	],
	"title.author.name": [
		"Auteur 1",
		"Auteur 2",
		"Auteur 3",
	],
	"title.author.viaf_id": [
		"Viaf A1",
		"Viaf A2",
		"Viaf A3",
	],
	"title.printer.name": [
		"Éditeur 1",
		"Éditeur 2",
	],
	"title.printer.viaf_id": [
		"Viaf E1",
		"Viaf E2",
	],
	"title.place.name": [
		"Ville 1",
	],
	"title.place.geonames_id": [
		"Geonames",
	],
	"title.istc_id": [
		"ISTC",
	],
	"title.witness.url": [
		"URL",
		"Note",
	],
	"title.name": [
		"Titre",
	],
	"table_type.dishas_id": [
		"dishas_id",
	],
	"table_type.table_edition": [
		"table_edition",
	],
	"table_type.astronomical_object.dishas_id": [
		"astronomical_object",
	],
	"table_type.name": [
		"name",
	],
	"table_type.chabas_number": [
		"chabas_number",
	],
	"table_type.chabas_name": [
		"chabas_name",
	],
	"table_type.chabas_description_page_start": [
		"chabas_description_page_start",
	],
	"table_type.chabas_description_page_end": [
		"chabas_description_page_end",
	],
};

# Main
def load_csv_file(filename: str) -> None:
	with open(filename) as csvfile:
		reader = csv.DictReader(csvfile);
		for row in reader:
			load_csv_row(**row);

def load_csv_row(**data) -> bool:
	title = get_title_from_csv_row(**data);
	if None != title:
		logger.info(f"Loaded Title '{title}'");
		sources = get_sources_from_csv_row(title, **data);
		return True;
	table_type = get_table_type_from_csv_row(**data);
	if table_type:
		logger.info(f"Loaded Table Type '{table_type}'");
		return True;
	return False;


# Extract bibliographical informations
def get_title_from_csv_row(**data) -> typing.Optional[Title]:
	title_name = get_from_csv_row("title.name", **data);
	if not title_name:
		return None;
	istc_id = get_from_csv_row("title.istc_id", **data);
	title = Title.objects.get_from_istc(istc_id);
	dishas_id = get_from_csv_row("title.dishas_id", **data);
	if None == title:
		title = Title.objects.get_from_heurist(dishas_id);
	title_place = get_place_from_csv_row(**data);
	title_date = get_from_csv_row("title.print_date", **data);
	if None == title:
		title = Title(name = title_name, place = title_place, print_date = title_date);
	hydrator.update_model_from_source_data(title, None, name=title_name, print_date=title_date);
	if None == title.place:
		title.place = title_place;
	authors_list = get_authors_from_csv_row(**data);
	printers_list = get_printers_from_csv_row(**data);
	for author in authors_list:
		if not author in title.authors.all():
			title.authors.add(author);
	for printer in printers_list:
		if not printer in title.printers.all():
			title.printers.add(printer);
	title.save();
	return title;


def get_sources_from_csv_row(title: Title, **data) -> list[Witness]:
	raw_url_list = get_from_csv_row("title.witness.url", **data)
	url_list = [url for url in raw_url_list if "http://" in url];
	return [Volume.objects.get_digital_witness_from_url(title, url) for url in url_list];


def get_authors_from_csv_row(**data) -> list[Actor]:
	return get_actors_from_csv_row("title.author", **data);

def get_printers_from_csv_row(**data) -> list[Actor]:
	return get_actors_from_csv_row("title.printer", **data);

def get_actors_from_csv_row(role: str, **data) -> list[Actor]:
	viaf_id_list = get_from_csv_row(f"{role}.viaf_id", unique = False, **data);
	names_list = get_from_csv_row(f"{role}.viaf_id", unique = False, **data);
	actors = [Actor.objects.get_from_viaf(viaf_id) for viaf_id in viaf_id_list];
	if len(viaf_id_list) == len(names_list):
		return actors;
	for name in names_list:
		actors.append(Actor.objects.get_from_name(name));
	return actor;


def get_place_from_csv_row(**data) -> typing.Optional[Place]:
	geonames_id = get_from_csv_row("title.place.geonames_id", **data);
	if geonames_id:
		return Place.objects.get_from_geonames(geonames_id);
	place_name = get_from_csv_row("title.place.name");
	if not place_name:
		return None;
	return Place.objects.get_or_create(name = place_name)[0];


# Extract table type informations
def get_table_type_from_csv_row(**data) -> typing.Optional[TableType]:
	dishas_id = get_from_csv_row("table_type.dishas_id", **data);
	table_type = get_table_type_from_dishas_id(dishas_id);
	fields_list = [
		"chabas_number",
		"chabas_name",
		"chabas_description_page_start",
		"chabas_description_page_end",
		"name",
		"table_edition",
	];
	informations = {};
	for field_name in fields_list:
		field_data = get_from_csv_row(f"table_type.{field_name}", **data);
		if None == field_data or "" == field_data.strip():
			continue;
		informations[field_name] = field_data;
	table_type = set_table_type_from_informations(table_type, **informations);
	if None == table_type:
		return None;
	name = get_from_csv_row("table_type.name", **data);
	table_type.name = name;
	table_type.save();
	return table_type;

def get_table_type_from_dishas_id(dishas_id: int = None) -> typing.Optional[TableType]:
	if dishas_id:
		return TableType.objects.get_from_dishas(dishas_id);
	return None;

def set_table_type_from_informations(table_type: TableType = None, **data) -> typing.Optional[TableType]:
	if len(data.keys()) < 1:
		return table_type;
	if None == table_type:
		return TableType.objects.get_or_create(**data)[0];

	for key, value in data.items():
		hydrator.update_property(table_type, key, value);

	table_type.save();
	return table_type;


# General extraction functions
def get_from_csv_row(key: str, unique: bool = True, **data) -> typing.Optional[typing.Union[str, list[str]]]:
	if len(data.keys()) < 1:
		logger.error(f"No data was passed");
		return None;
	if not key in CSV_TO_KEY.keys():
		logger.error(f"Tried to read unknown key '{key}'");
		return None;
	results = None if unique else [];
	for possible_key in CSV_TO_KEY[key]:
		if possible_key in data.keys() and unique:
			return data[possible_key];
		if possible_key in data.keys():
			results.append(data[possible_key]);
	return results;


if "django.core.management.commands.shell" == __name__:
	load_csv_file("handmade_corpus.csv")


