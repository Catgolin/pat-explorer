import logging;

from works.models import Corpus;
from scenery.models import Place;
from source_materials.models import Witness;

logger = logging.getLogger(__name__);

def create_corpuses():
	corpuses_list = [
		Corpus(
			name = "Parisian Alfonsine Tables",
			rules = "alfons\nalphons\nmuris",
			composition_date = 1231,
			place = Place.objects.get_from_geonames(2988507),
		),
		Corpus(
			name = "Tabule resolute",
			rules = "resolut\nschoner",
			composition_date = "c.1380",
		),
		Corpus(
			name = "Elisabeth Regine",
			rules = "elisabeth",
			composition_date = 1503,
			place = Place.objects.get_from_geonames(3169071),
		),
		Corpus(
			name = "Bianchini",
			rules = "bianchini\nblanchini\nprimi",
			composition_date = "c.1450",
			place = Place.objects.get_from_geonames(3177090),
		),
		Corpus(
			name = "Tabulae Eclypsium",
			rules = "eclypsium",
			composition_date = "c.1460",
			place = Place.objects.get_from_geonames(2761369),
		),
		Corpus(
			name = "Tabulae directionum",
			rules = "direction",
			composition_date = "1467",
			place = Place.objects.get_from_geonames(3054667),
		),
		Corpus(
			name = "Almanachs, Calendars",
			rules = "efemeri\nephemeri\nalmanach\nkalendar\ncalendar",
		),
		Corpus(
			name = "Disputatio, Comentarius",
			rules = "commentar\ndisputatio",
		),
		Corpus(
			name = "Equatoires",
			rules = "eqvator\nequator",
		),
		Corpus(
			name = "Eclipse",
			rules = "eclipse",
		),
		Corpus(
			name = "Theorice",
			rules = "theoric",
		),
	];
	for corpus in corpuses_list:
		corpus.save();


def main():
	logger.info("Loading corpuses...");
	create_corpuses();
	logger.info("Loading data from heurist...");
	Witness.objects.get_all_from_heurist();
	from load_from_handmade_csv import load_csv_file;
	load_csv_file("handmade_corpus.csv");
	load_csv_file("table_types.csv");

if "django.core.management.commands.shell" == __name__:
	main();
