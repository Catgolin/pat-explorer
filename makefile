test:
	python3 manage.py test --failfast --exclude-tag slow --parallel 2

complete-test:
	python3 manage.py test --parallel 2

repopulate:
	rm db.sqlite3;
	python3 manage.py migrate;
	python3 manage.py createsuperuser --noinput --username root --email exemple@email.com
	python3 manage.py shell < populate.py;
