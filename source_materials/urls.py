from django.urls import path;

from . import views;

app_name = "sources";

urlpatterns = [
	path('', views.CorpusIndexView.as_view(), name="index"),
	path('title/<int:pk>', views.TitleDetailView.as_view(), name="title"),
	path('witness/<int:pk>', views.WitnessDetailView.as_view(), name="witness"),
];

