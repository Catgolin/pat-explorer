import re;
import logging;

from urllib.parse import urlparse;
from asgiref import sync;

from django.db.models import Manager;
from heurist import basic_queries, const;

from commons import network, hydrator;
from works.models import Title;

from . import models;

logger = logging.getLogger(__name__)

class LibraryManager(Manager):
	def get_from_cerl(self, cerl_id: str, force_update: bool = False):
		""" """
		if not cerl_id:
			return None;

		data = self.get_informations_from_cerl(cerl_id);

		if not data:
			return None;

		library = self.get_or_create(cerl_id=cerl_id)[0];
		hydrator.update_model_from_source_data(library, data[0], force_update = force_update, **data[1]);

		return library;

	def get_informations_from_cerl(self, cerl_id: str, force_update: bool = False) -> tuple[str, dict]:
		if not cerl_id:
			return None;

		url = f"https://data.cerl.org/holdinst/{cerl_id}";
		params = {"format": "json"};
		data = sync.async_to_sync(network.get_json)(url, params, field="data");
		if None == data:
			logger.warning(f"No data for holdingst with id {cerl_id}");
			return None;
		return ("data.cerl.org", data);

class WitnessManager(Manager):
	def get_all_from_istc(self, istc_id: str) -> list:
		data = Title.objects.get_informations_from_istc(istc_id);
		if not data:
			return None;
		title = Title.objects.get_title_from_istc_data(istc_id, **data[1]);
		digital = self.get_digital_reproductions_from_istc_data(title, data[1]);
		physical = self.get_physical_reproductions_from_istc_data(title, data[1]);
		return digital + physical;


	def get_digital_reproductions_from_istc(self, istc_id: str) -> list:
		data = Title.objects.get_informations_from_istc(istc_id);
		if not data:
			return None;
		title = Title.objects.get_title_from_istc_data(istc_id, **data[1]);
		return self.get_digital_reproductions_from_istc_data(title, data[1]);


	def get_digital_reproductions_from_istc_data(self, title: Title, data: dict) -> list:
		if not data or not "related_resources" in data.keys():
			return None;

		digital_witnesses = [];
		for resource in data["related_resources"]:
			if not "digital" == resource["resource_type"]:
				continue;
			url = resource['resource_url'];
			witness = self.get_digital_witness_from_url(title, url, source="data.cerl.org");
			witness.name = resource['resource_name'];
			hydrator.update_source_of_model(witness, "data.cerl.org", "name");
			digital_witnesses.append(witness);
		return digital_witnesses;

	def get_physical_reproductions_from_istc_data(self, title: Title, data: dict) -> list:
		if not data or not "holdings" in data.keys():
			return None;

		physical_witnesses = [];
		for holding_data in data["holdings"]:
			holdinst_id = holding_data['holding_institution_id'].replace(' ', '');
			library = models.Library.objects.get_from_cerl(holdinst_id);
			if None == library:
				continue;
			witness = self.get_or_create(title=title, library=library)[0];
			if "shelf_mark" in holding_data:
				witness.shelf_mark = holding_data["shelf_mark"];
				hydrator.update_source_of_model(witness, "data.cerl.org", "shelf_mark");
			hydrator.update_source_of_model(witness, "data.cerl.org", "title");
			hydrator.update_source_of_model(witness, "data.cerl.org", "library");
			hydrator.update_source_of_model(witness, "data.cerl.org", "witness");
			physical_witnesses.append(witness);
		return physical_witnesses;


	def get_digital_witness_from_url(self, title: Title, url: str, source: str = "???"):
		if not title:
			return None;
		if not url:
			return None;
		if "urn:" in url:
			urn = get_urn_from_url(url);
			witness = self.get_or_create(title=title, urn=urn)[0];
			witness.is_digital = True;
			hydrator.update_source_of_model(witness, source, "urn");
			return witness;
		witness = self.get_or_create(title=title, url=url)[0];
		witness.is_digital = True;
		hydrator.update_source_of_model(witness, source, "url");
		return witness;


	def get_all_from_heurist(self, type_of_items: int = const.ED) -> list:
		records_list = basic_queries.get_type_records(type_of_items);
		all_witnesses = []
		for i, item in enumerate(records_list):
			print(f"\rLoading titles from heurist ({i}/{len(records_list)})...", end="")
			data = network.get_dictionary_from_heurist_list(*item["details"]);
			witnesses_list = self.get_from_heurist_data(item["rec_ID"], **data);
			if None == witnesses_list:
				continue;
			all_witnesses += witnesses_list;
		return all_witnesses;

	def get_from_heurist(self, hid: int) -> list:
		return self.get_from_heurist_data(hid, **network.get_data_from_heurist_id(hid));

	def get_from_heurist_data(self, hid: int, **data) -> list:
		title = Title.objects.get_from_heurist_data(hid, **data);
		if None == title:
			return None;

		if not "Digitization URL" in data.keys() and not "IIIF Manifest" in data.keys():
			witness = self.get_or_create(title=title, dishas_id=hid)[0];
			hydrator.update_model_from_source_data(witness, "heurist.huma-num.fr", **data);
			return [witness];

		witnesses = [];

		manifests = [];
		if "IIIF Manifest" in data.keys():
			manifests = data["IIIF Manifest"].split("\n");
			del data["IIIF Manifest"];

		if "Digitization URL" in data.keys():
			urls = data["Digitization URL"].split("\n");
			del data["Digitization URL"]
			for url in urls:
				source = self.get_from_heurist_through_url(hid, title, url, manifests, **data);
				witnesses.append(source);

		for manifest in manifests:
			source, created = self.get_or_create(title=title, iiif_manifest = manifest);
			hydrator.update_source_of_model(source, "heurist.huma-num.fr", "iiif_manifest");
			source.dishas_id = hid;
			hydrator.update_model_from_source_data(source, "heurist.huma-num.fr", **data);
			if not source in witnesses:
				witnesses.append(source);

		return witnesses;

	def get_from_heurist_through_url(self, hid: int, title: Title, url: str, manifest_candidates: list[str] = [], **data):
		source = self.get_digital_witness_from_url(title, url, source = "heurist.huma-num.fr");
		source.dishas_id = hid;
		hydrator.update_model_from_source_data(source, "heurist.huma-num.fr", **data);
		for manifest in manifest_candidates:
			if get_host_from_url(url) == get_host_from_url(manifest):
				manifest_data = {"IIIF Manifest": manifest};
				hydrator.update_model_from_source_data(source, "heurist.huma-num.fr", **manifest_data);
		return source;


def get_urn_from_url(url: str) -> str:
	if not url or not "urn:" in url:
		return None;
	match = re.search("urn:[\w:\-_]*:[\w:\-_]*", url);
	if match:
		return match.group(0);
	return None;

def get_host_from_url(url: str) -> str:
	hostname_parts = urlparse(url).hostname.split(".");
	if len(hostname_parts) > 2:
		return ".".join(hostname_parts[-2:]);
	return ".".join(hostname_parts);
