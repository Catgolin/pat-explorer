from urllib.parse import urlparse;

from django.db import models;
from django.utils.translation import gettext_lazy as _;

from scenery.models import Place;
from works.models import Title;

from .managers import LibraryManager, WitnessManager;

class Library(models.Model):
	""" Stores information about a library, or other holding site of a printed witness. """

	@staticmethod
	def ontology():
		return {
			"keys": {
				"cerl_id": "data.cerl.org",
				"dishas_id": "dishas.obspm.fr",
			},
			"sources": {
				"data.cerl.org": {
					"_id": "cerl_id",
					"name": "name",
					"geonamesId": "location",
					"country": "country_code",
					"isil": "isil",
				},
				"dishas.obspm.fr": {
					"kibana_name": "name",
					"kibana_id": "dishas_id",
				},
			},
		};

	objects = LibraryManager();

	class Meta:
		verbose_name = _("library");
		verbose_name_plural = _("libraries");


	#   ===   Database informations   ===
	dishas_id = models.CharField(
		_("Dishas' identifier"),
		max_length = 100,
		null = True, unique = True,
		blank = True,
		help_text = _("Identifier of the library in the dishas' elastic-search API"),
	);

	cerl_id = models.CharField(
		_("cerl.org identifier"),
		max_length = 200,
		null = True, unique = True,
		blank = True,
		help_text = _("Identifier of the library on data.cerl.org/holdinst"),
	);

	#   ===   Basic informations   ===
	name = models.CharField(
		_("name"),
		max_length = 200,
		blank = True,
		help_text = _("Name of the library"),
	);

	location = models.ForeignKey(
		Place,
		verbose_name=_("location"),
		on_delete = models.CASCADE,
		null = True, blank = True,
		help_text = _("Where the library is situated"),
	);

	country_code = models.CharField(
		_("country code"),
		max_length = 3,
		blank = True,
		help_text = _("Nationality of the library following ISO 3166-1 alpha convention"),
	);

	isil = models.CharField(
		_("isil number"),
		max_length = 16,
		blank = True,
		help_text = _("International Standard Identifier for Libraries and Related Organizations, norm ISO 15511"),
	);


	#   ===   Sources   ===
	data_sources = models.TextField(
		_("sources of the informations"),
		blank=True,
		help_text = _("Source of the informations stored about this library"),
	);


	class Meta:
		verbose_name = "library";
		verbose_name_plural = "libraries";

	def __str__(self):
		return self.name;


class Witness(models.Model):
	""" Informations about one particular version of an eddited title """

	@staticmethod
	def ontology():
		return {
			"keys": {},
			"sources": {
				"heurist.huma-num.fr": {
					"IIIF Manifest": "iiif_manifest",
					"Digitization URL": "url",
					"Catalog URL": "catalog_url",
				},
			},
		};

	objects = WitnessManager();

	class Meta:
		verbose_name = _("witness");
		verbose_name_plural = _("witnesses");

	#   ===   Database links   ===
	dishas_id = models.IntegerField(
		_("Dishas identifier"),
		null = True, blank = True,
		help_text = _("Identifier of the source in the dishas' huma-num database"),
	);

	#   ===   Identification informations   ===
	name = models.CharField(
		max_length = 100,
		blank = True,
		help_text = _("Name of this particular witness"),
	);

	title = models.ForeignKey(
		Title,
		verbose_name =_("title"),
		on_delete=models.CASCADE,
		related_name="witnesses",
		help_text = _("The historical edition of this witness"),
	);

	#   ===   Localization informations   ===
	library = models.ForeignKey(
		Library,
		verbose_name=_("holding site"),
		on_delete=models.CASCADE,
		related_name="holdings",
		null=True, blank = True,
		help_text=_("Institution holding this particular witness"),
	);
	shelf_mark = models.CharField(
		_("shelf mark"),
		max_length=100,
		blank=True,
		help_text=_("Identifier of this witness in the holding site's catalogue"),
	);
	catalog_url = models.CharField(
		_("catalog's url"),
		max_length = 100,
		blank = True,
		help_text = _("Location of this witness in a catalogue"),
	);

	#   ===   Digitalization informations   ===
	is_digital = models.BooleanField(
		_("is digital"),
		default = False,
		help_text=_("Is this witness available in digital format"),
	);
	url = models.CharField(
		_("URL"),
		max_length = 200,
		blank=True,
		help_text=_("Location of a digitalized version of this witness"),
	);
	urn = models.CharField(
		_("URN"),
		max_length = 200,
		blank=True,
		help_text=_("Unified Resource Name of a digitalized version of this witness"),
	);
	iiif_manifest = models.CharField(
		_("IIIF manifest"),
		max_length = 200,
		blank=True,
		help_text=_("International Image Interoperability manifest's URL"),
	);

	notes = models.TextField(
		_("notes"),
		blank=True,
		help_text = _("Free notes about this particular witness"),
	);

	#   ===   Sources   ===
	data_sources = models.TextField(
		_("sources of the informations"),
		blank=True,
		help_text = _("Source of the informations stored about this library"),
	);


	def __str__(self):
		if self.name:
			return self.name;
		if self.shelf_mark and self.library.__str__():
			library_name = self.library.__str__();
			return f"{self.shelf_mark} | {library_name}";
		if self.iiif_manifest or self.url or self.urn:
			return "Digital copy";
		return f"Witness of {self.title.name}";

	def get_link(self):
		if self.iiif_manifest:
			return f"https://dishas.obspm.fr/alfa/mirador?manifest={self.iiif_manifest}";
		if self.url:
			return self.url;
		if self.urn:
			return f"https://nbn-resolving.org/process-urn-form?identifier={self.urn}&verb=REDIRECT";
		return None
