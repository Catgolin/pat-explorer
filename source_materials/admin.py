from django.contrib import admin;

from .models import Library, Witness;

admin.site.register(Library);
admin.site.register(Witness);

