from django.shortcuts import render;
from django.views import generic;

import logging;

from works.models import Title, Corpus;
from .models import Witness, Library;

logger = logging.getLogger(__name__);

class CorpusIndexView(generic.ListView):
	context_object_name = "corpuses_list";
	template_name = "source_materials/corpus_list.html";

	def get_queryset(self):
		return Corpus.objects.all().order_by("composition_date", "name");

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs);
		context["alone_titles"] = Title.objects.filter(corpus__isnull=True).order_by("print_date");
		return context;

class TitleDetailView(generic.DetailView):
	model = Title;
	template_name = "source_materials/title_detail.html"

class WitnessDetailView(generic.DetailView):
	model = Witness;
