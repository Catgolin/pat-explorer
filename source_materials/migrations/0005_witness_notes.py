# Generated by Django 4.2 on 2023-05-04 12:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('source_materials', '0004_alter_library_location_alter_witness_dishas_id_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='witness',
            name='notes',
            field=models.TextField(blank=True, help_text='Free notes about this particular witness', verbose_name='notes'),
        ),
    ]
