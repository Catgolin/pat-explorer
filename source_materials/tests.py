import asyncio;

from unittest import skip;

from django.test import TestCase, tag;

from commons.tests import assert_data_sourced;
from works.models import Title, Corpus;

from .models import Library, Witness;

class HoldingSitesTest(TestCase):
	def test_get_holding_site_from_cerl(self):
		library = Library.objects.get_from_cerl("MunchenBSB");
		self.assertEqual(library.name, "München, Bayerische Staatsbibliothek");
		self.assertEqual(library.country_code, "DE");
		self.assertEqual(library.isil, "DE-12");
		self.assertEqual(library.location.geonames_id, "2867714");
		sourced_properties = ["name", "country_code", "location"];
		assert_data_sourced(library, sourced_properties, "data.cerl.org");


class WitnessTest(TestCase):
	def test_get_all_sources_from_istc(self):
		sources = Witness.objects.get_all_from_istc("ir00112500");
		self.assertEqual(len(sources), 2);
		physical_sources = Witness.objects.filter(is_digital = False);
		self.assertEqual(physical_sources.count(), 1);
		self.assertEqual(physical_sources.first().library.cerl_id, "MunchenBSB");
		self.assertEqual(physical_sources.first().library.isil, "DE-12");
		digital_sources = Witness.objects.filter(is_digital = True);
		self.assertEqual(digital_sources.count(), 1);
		self.assertEqual(digital_sources.first().urn, "urn:nbn:de:bvb:12-bsb00044153-2");
		for witness in sources:
			with self.subTest(witness=witness):
				self.assertEqual(witness.title.istc_id, "ir00112500");

	def test_get_electronic_sources_from_istc(self):
		sources = Witness.objects.get_digital_reproductions_from_istc("ia00534000");
		self.assertEqual(len(sources), 2);
		self.assertEqual(sources[0].urn, "urn:nbn:de:bvb:12-bsb00060314-6");
		self.assertEqual(sources[1].url, "http://dlib.gnm.de/item/8Inc34039");
		assert_data_sourced(sources[0], ["name", "urn"], "data.cerl.org");
		assert_data_sourced(sources[1], ["name", "url"], "data.cerl.org");

	@skip("Not implemented")
	def test_get_data_from_iiif_manifest(self):
		istc_id = "ia00534000";
		book = Title.objects.get_from_istc(istc_id);
		iiif = "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb00060314/manifest";
		source = Source.objects.get_or_create(title=book, iiif_manifest=iiif)[0];
		self.assertEqual(source.shelf_mark, "4 Inc.c.a. 380");
		self.assertEqual(source.urn, "urn:nbn:de:bvb:12-bsb00060314-6");


class LoadFromSurvey(TestCase):
	def test_get_source_from_heurist(self):
		sources = Witness.objects.get_from_heurist(4114);
		self.assertEqual(len(sources), 1, sources);
		source = sources[0];
		# Assert the Title was created
		self.assertEqual(source.title.name, "Tabulae astronomicae. Add: Johannes Danck: Canones in tabulas Alphonsi");
		self.assertEqual(source.title.istc_id, "ia00534000");
		self.assertEqual(source.title.print_date, 1483);
		self.assertEqual(source.title.authors.count(), 2);
		self.assertEqual(source.title.authors.all()[1].viaf_id, "316880574");
		self.assertEqual(source.title.printers.count(), 1);
		self.assertEqual(source.title.printers.first().viaf_id, "25398656");
		self.assertEqual(source.title.place.geonames_id, 3164603);
		# self.assertEqual(source.title.place.dishas_id, "858");
		self.assertAlmostEqual(source.title.place.longitude, 12.327036, delta=1);
		self.assertAlmostEqual(source.title.place.latitude, 45.436087, delta=1);
		sourced_data = ["name", "print_date"];
		assert_data_sourced(source.title, sourced_data, "data.cerl.org");
		assert_data_sourced(source.title, ["istc_id"], "heurist.huma-num.fr");
		# Assert the Witness was created
		self.assertEqual(source.catalog_url, "https://data.cerl.org/istc/ia00534000");
		self.assertEqual(source.url, "https://daten.digitale-sammlungen.de/~db/0006/bsb00060314/images/");
		self.assertEqual(source.iiif_manifest, "https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb00060314/manifest");
		sourced_data = ["catalog_url", "url", "iiif_manifest"];
		assert_data_sourced(source, sourced_data, "heurist.huma-num.fr");

	def test_get_source_from_heurist_without_istc(self):
		sources = Witness.objects.get_from_heurist(6765);
		source = sources[0];
		self.assertEqual(source.title.dishas_id, 6765);
		self.assertEqual(source.title.name, "Tabulae Alphonsinae perpetuae motuum coelestium");
		self.assertEqual(source.title.print_date, 1641);
		self.assertEqual(source.title.authors.count(), 1);
		self.assertEqual(source.title.authors.first().viaf_id, "66476694");
		self.assertEqual(source.title.printers.count(), 1);
		self.assertEqual(source.title.printers.first().name, "Officina Regia");
		self.assertEqual(source.title.printers.first().viaf_id, None);
		sourced_data = ["name", "print_date"];
		assert_data_sourced(source.title, sourced_data, "heurist.huma-num.fr");
		self.assertEqual(source.catalog_url, "https://www.worldcat.org/title/tabulae-alphonsinae-perpetuae-motuum-coelestium/oclc/52533422");
		self.assertEqual(source.url, "");
		assert_data_sourced(source, ["catalog_url"], "heurist.huma-num.fr");

	def test_get_source_from_heurist_with_multiple_urls(self):
		sources = Witness.objects.get_from_heurist(4118);
		self.assertEqual(len(sources), 4);
		for source in sources:
			with self.subTest(source=source):
				self.assertEqual(source.title.istc_id, "ia00535000");
		sources_url = [source.url for source in sources if "" != source.url];
		sources_manifest = [source.iiif_manifest for source in sources if "" != source.iiif_manifest];
		self.assertEqual(len(sources_url), 3);
		self.assertEqual(len(sources_manifest), 1);
		expected_urls = [
			"http://www.bibliotecavirtualdeandalucia.es/catalogo/catalogo_imagenes/grupo.cmd?path=10019",
			"http://www.bibliotecavirtualdeandalucia.es/catalogo/catalogo_imagenes/grupo.cmd?path=10032",
			"https://daten.digitale-sammlungen.de/~db/0004/bsb00048395/images/",
		];
		expected_iiif_manifest = "https://lib.is/IE6624180/manifest?manifest=https://lib.is/IE6624180/manifest"
		for url in expected_urls:
			with self.subTest(url=url):
				assert url in sources_url, f"Missing {url} in {sources_url}";
		assert expected_iiif_manifest in sources_manifest;


	@tag("slow")
	def test_get_all_sources_from_heurist(self):
		pat = Corpus(name="Parisian Alfonsine Tables", rules="alfons\nalphons\nmuris");
		pat.save();
		sources = Witness.objects.get_all_from_heurist();
		self.assertEqual(Title.objects.count(), 126);
		self.assertGreater(len(sources), 127);
		self.assertEqual(pat.titles.count(), 7);
