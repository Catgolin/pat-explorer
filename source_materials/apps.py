from django.apps import AppConfig


class SourceMaterialsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'source_materials'
