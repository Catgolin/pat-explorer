from django.urls import path;

from . import views;

app_name = "works"

urlpatterns = [
	path('', views.CorpusIndexView.as_view(), name="index"),
	path('corpus/<int:pk>', views.CorpusDetailView.as_view(), name="corpus"),
	path('title/<int:pk>', views.TitleDetailView.as_view(), name="title"),
	path('actor/<int:pk>', views.ActorDetailView.as_view(), name="actor"),
];
