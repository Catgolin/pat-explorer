import flatdict;
import logging;

from urllib.parse import urlparse;
from heurist import const;
from asgiref import sync;

from django.db.models import Manager;
from heurist import basic_queries, const;

from commons import network, hydrator;
from scenery.models import Actor;
from scenery.managers import get_year_from_string;

from . import models;

logger = logging.getLogger(__name__);

class TitleManager(Manager):

	def all(self, *args, **kwargs):
		return super().all(*args, **kwargs).order_by("print_date");

	def get_from_heurist(self, hid: int, force: bool = False):
		return self.get_from_heurist_data(hid, force=force, **network.get_data_from_heurist_id(hid));

	def get_from_heurist_data(self, hid: int, force: bool = False, **data):
		if not "Title" in data.keys():
			return None;

		title = None;

		if "Catalog URL" in data.keys():
			parsed_url = urlparse(data["Catalog URL"]);
			path = parsed_url.path.strip("/").split("/");
			if 2 == len(path) and "istc" == path[0]:
				title = self.get_from_istc(path[1]);
				hydrator.update_source_of_model(title, "heurist.huma-num.fr", "istc_id");
				title.dishas_id = hid;

		if None == title:
			title = self.get_or_create(dishas_id = hid)[0];

		print_year = get_year_from_string(data["Date"]);
		tempus_ante_quem = "";
		if not data["Date"] == print_year:
			tempus_ante_quem = data["Date"][-4:];
		data["Date"] = print_year;
		data["Date_ante_quem"] = tempus_ante_quem;
		hydrator.update_model_from_source_data(title, "heurist.huma-num.fr", **data);

		if "Historical actor" in data.keys():
			author_name = data["Historical actor"];
			if title.authors.through.objects.filter(printer=None, designation=author_name).count() < 1:
				authors = Actor.objects.get_from_names(author_name, activity_date = data["Date"], separators=[",", "and", "\n"]);
				for author in authors:
					title.authors.add(author, through_defaults={
						'designation': author_name,
						'attributor': "dishas.obspm.fr/alfa",
					});

		if "Editor" in data.keys():
			printer_name = data["Editor"];
			if title.printers.through.objects.filter(author=None, designation=printer_name).count() < 1:
				printers = Actor.objects.get_from_names(printer_name, activity_date = data["Date"], alive_while_active = False, separators=[",", "and", "\n", " ans "]);
				for printer in printers:
					title.printers.add(printer, through_defaults={
						'designation': printer_name,
						'attributor': "dishas.obspm.fr/alfa",
					});

		models.Corpus.objects.add_title_to_corpuses(title);

		return title;

	def get_from_istc(self, istc_id: str, force: bool = False):
		""" Creates a Title using informations from CERL's ISTC catalogue. """
		if not istc_id:
			return None;

		data = self.get_informations_from_istc(istc_id);

		if not data:
			return None;

		return self.get_title_from_istc_data(istc_id, data[0], force=force, **data[1]);

	def get_title_from_istc_data(self, istc_id: str, source: str="data.cerl.org", force: bool = False, **data):
		""" Transforms an array representing ISTC informations into a Title instance """
		if not istc_id:
			return None;

		title = self.get_or_create(istc_id = istc_id)[0];
		hydrator.update_model_from_source_data(title, source, force_update = force, **data);
		hydrator.update_source_of_model(title.place, source, "geonames_id");

		date_start = data["date_of_item_single_date"] if "date_of_item_single_date" in data.keys() else data["date_of_item_from"];
		date_end = data["date_of_item_single_date"] if "date_of_item_single_date" in data.keys() else data["date_of_item_to"];
		if "imprint.imprint_name" in data.keys() and data["imprint.imprint_name"]:
			printers = Actor.objects.get_from_names(data["imprint.imprint_name"], activity_date = date_end, alive_while_active = True);
			for printer in printers:
				title.printers.add(printer, through_defaults={
					'designation': self.format_name(data["imprint.imprint_name"]),
					'attributor': f"data.cerl.org/istc/{istc_id}",
				});
		if "author" in data.keys() and data["author"]:
			authors = Actor.objects.get_from_names(data["author"], activity_date = date_end, alive_while_active = False);
			for author in authors:
				title.authors.add(author, through_defaults= {
					'designation': self.format_name(data["author"]),
					'attributor': f"data.cerl.org/istc/{istc_id}",
				});

		models.Corpus.objects.add_title_to_corpuses(title);

		return title;

	def get_informations_from_istc(self, istc_id: str) -> tuple[str, dict]:
		""" Returns informations about a title taken from the CERL's ISTC catalogue.
		In the returned tuple:
		the first element indicates the source of the information,
		the second element is a dictionary represneting the informations about the title. """
		if not istc_id:
			return False;

		url = "https://data.cerl.org/istc/_search";
		params = {
			"format": "txt",
			"query": istc_id,
		};

		data = sync.async_to_sync(network.get_yaml)(url, params);
		if None == data:
			logger.warning(f"Invalid data from ISTC number '{istc_id}'");
			return None;

		data["imprint"] = data["imprint"][0];
		data["imprint"]["geo_info"] = data["imprint"]["geo_info"][0];
		data["references"] = [ref["reference"] for ref in data["references"]];
		data["gw_ref"] = next(ref.split(" ")[1] for ref in data["references"] if "GW" == ref.split(" ")[0]);
		flatten_data = flatdict.FlatDict(data, delimiter=".");
		return ("data.cerl.org", dict(flatten_data));

	def format_name(self, raw_name: str) -> str:
		""" Removes parethesis, brackets and other unwanted characters around strings """
		if raw_name.strip("[") != raw_name and raw_name.strip("]") != raw_name:
			raw_name = raw_name.strip("[").strip("]");
		if raw_name.strip("(") != raw_name and raw_name.strip(")") != raw_name:
			raw_name = raw_name.strip("(").strip(")");
		return raw_name;


class CorpusManager(Manager):

	def add_titles_to_corpuses(self):
		""" Populate all the corpuses of this database with every matching titles """
		for corpus in self.all():
			corpus.add_titles_matching_rules();

	def add_title_to_corpuses(self, title):
		""" Adds the given title to all matching corpuses """
		for corpus in self.all():
			if corpus.does_title_belong(title):
				corpus.titles.add(title);


class AttributionManager(Manager):
	def all(self, *args, **kwargs):
		return super().all(*args, **kwargs).order_by("title__print_date");
