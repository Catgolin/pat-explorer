from unittest import skip;

from django.test import TestCase

from scenery.models import Actor, Place;
from .models import Title, Corpus;

from commons.tests import assert_data_sourced;

class ISTCTitleTest(TestCase):
	""" Tests for getting titles from the Incunabala Short Title Catalogue """
	def test_get_title_from_istc(self):
		title = Title.objects.get_from_istc("ia00534000");
		# Book informations
		self.assertEqual(title.istc_id, "ia00534000");
		self.assertEqual(title.name, "Tabulae astronomicae. Add: Johannes Danck: Canones in tabulas Alphonsi");
		self.assertEqual(title.gw_id, "1257");
		self.assertEqual(title.dimensions, "4°");
		self.assertEqual(title.print_date, 1483);
		self.assertEqual(title.place.geonames_id, 3164603);
		# Attributions: author
		self.assertEqual(title.authors.count(), 1);
		self.assertEqual(title.authors.first().viaf_id, "66476694");
		author_attribution = title.authors.through.objects.filter(printer=None).first();
		self.assertEqual(author_attribution.designation, "Alphonsus, Rex Castellae");
		self.assertEqual(author_attribution.attributor, "data.cerl.org/istc/ia00534000");
		# Attributions: printer
		self.assertEqual(title.printers.count(), 1);
		self.assertEqual(title.printers.first().viaf_id, "25398656");
		printer_attribution = title.printers.through.objects.filter(author=None).first();
		self.assertEqual(printer_attribution.designation, "Erhard Ratdolt");
		self.assertEqual(printer_attribution.attributor, "data.cerl.org/istc/ia00534000");
		# Sources
		sourced_properties = ["name", "dimensions", "print_date", "place", "gw_id"];
		assert_data_sourced(title, sourced_properties, "data.cerl.org");
		assert_data_sourced(title.place, ["geonames_id"], "data.cerl.org");
		sourced_properties = ["name", "latitude", "longitude"];
		assert_data_sourced(title.place, sourced_properties, "geonames.org");

	def test_get_title_from_istc_bis(self):
		title = Title.objects.get_from_istc("ia00537500");
		# Book informations
		self.assertEqual(title.istc_id, "ia00537500");
		self.assertEqual(title.name, "Lumen caeli seu expositio instrumenti astronomici a se excogitati");
		self.assertEqual(title.gw_id, "1573");
		self.assertEqual(title.dimensions, "f°");
		self.assertEqual(title.print_date, 1498);
		self.assertEqual(title.place.geonames_id, 3169071);
		# Attributions: author
		self.assertEqual(title.authors.count(), 1);
		self.assertEqual(title.authors.first().viaf_id, "24145911007427061382");
		# self.assertEqual(title.authors.first().viaf_id, "280836932");
		author_attribution = title.authors.through.objects.filter(printer=None).first();
		self.assertEqual(author_attribution.designation, "Alphonsus de Corduba");
		self.assertEqual(author_attribution.attributor, "data.cerl.org/istc/ia00537500");
		# Attributions: printer
		self.assertEqual(title.printers.count(), 1);
		self.assertEqual(title.printers.first().viaf_id, "72599933");
		printer_attribution = title.printers.through.objects.filter(author=None).first();
		self.assertEqual(printer_attribution.designation, "Johann Besicken");
		self.assertEqual(printer_attribution.attributor, "data.cerl.org/istc/ia00537500");
		# Sources
		sourced_properties = ["name", "dimensions", "print_date", "place", "gw_id"];
		assert_data_sourced(title, sourced_properties, "data.cerl.org");
		assert_data_sourced(title.place, ["geonames_id"], "data.cerl.org");
		sourced_properties = ["name", "latitude", "longitude"];
		assert_data_sourced(title.place, sourced_properties, "geonames.org");

	def test_get_informations_from_istc(self):
		res = Title.objects.get_informations_from_istc("ia00534000");
		self.assertEqual(res[0], "data.cerl.org");
		data = res[1];
		self.assertEqual(data["title"], "Tabulae astronomicae. Add: Johannes Danck: Canones in tabulas Alphonsi");
		self.assertEqual(data["author"], "Alphonsus, Rex Castellae");
		self.assertEqual(data["date_of_item_single_date"], 1483);
		self.assertEqual(data["dimensions"], "4°");
		self.assertEqual(data["imprint.geo_info.geonames_id"], 3164603);
		self.assertEqual(data["imprint.imprint_name"], "Erhard Ratdolt");
		self.assertEqual(data["gw_ref"], "1257");


	def test_title_from_false_istc(self):
		title = Title.objects.get_from_istc("NotAnIstcNumber")
		self.assertEqual(title, None)


	def test_no_duplicate_from_istc_when_place_and_author_in_db(self):
		venice = Place.objects.get_from_geonames(geonames_id = 3164603);
		ratdolt = Actor.objects.get_from_viaf(25398656)
		alfonse = Actor.objects.get_from_viaf(66476694)
		title = Title.objects.get_from_istc("ia00534000")
		self.assertEqual(title.place, venice)
		self.assertEqual(title.printers.first(), ratdolt)
		self.assertEqual(title.authors.first(), alfonse)


class TitleToCorpusTest(TestCase):
	""" Tests for the attribution of titles to corpuses """

	def test_new_title_included_in_corpus(self):
		pat = Corpus(rules="alfons\nalphons\nmuris");
		pat.save();
		title = Title.objects.get_from_istc("ia00534000");
		self.assertEqual(pat.titles.count(), 1);
		self.assertEqual(pat.titles.first(), title);

	def test_previous_title_included_in_corpus(self):
		title = Title.objects.get_from_istc("ia00534000");
		pat = Corpus(rules="alfons\nalphons\nmuris");
		pat.save();
		self.assertEqual(pat.titles.count(), 1);
		self.assertEqual(pat.titles.first(), title);

	def test_unrelated_title_not_added(self):
		pat = Corpus(rules="calendar\nkalendar");
		pat.save();
		title = Title.objects.get_from_istc("ia00534000");
		self.assertEqual(pat.titles.count(), 0);


class TitleFromHeuristTest(TestCase):
	def test_new_title_from_heurist(self):
		title = Title.objects.get_from_heurist(7113);
		self.assertEqual(title.name, "Tabule astronomice diui Alfonsi Regis Romanorum et Castille");
		self.assertEqual(title.print_date, 1518);
		self.assertEqual(title.authors.count(), 0);
		self.assertEqual(title.printers.count(), 1);
		self.assertEqual(title.dishas_id, 7113);

	def test_new_title_from_heurist_and_istc(self):
		title = Title.objects.get_from_heurist(4114);
		self.assertEqual(title.name, "Tabulae astronomicae. Add: Johannes Danck: Canones in tabulas Alphonsi");
		self.assertEqual(title.istc_id, "ia00534000");
		self.assertEqual(title.dishas_id, 4114);
		self.assertEqual(title.authors.count(), 2);
		self.assertEqual(title.printers.count(), 1);

	def test_new_title_from_heurist_bis(self):
		title = Title.objects.get_from_heurist(5187);
		self.assertEqual(title.authors.count(), 2);
