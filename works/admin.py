import logging;

from django.contrib import admin;

from .models import Corpus, Title, Attribution;

logger = logging.getLogger(__name__);

@admin.register(Corpus)
class CorpusAdmin(admin.ModelAdmin):
	filter_horizontal = ["titles", "inspired"];

class AttributionInline(admin.StackedInline):
	fields = [("author", "printer"), ("designation", "attributor"), "notes"];
	model = Attribution;
	extra = 0;

@admin.register(Title)
class TitleAdmin(admin.ModelAdmin):
	inlines = [AttributionInline]
