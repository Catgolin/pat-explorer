from django.db import models;
from django.utils.translation import gettext_lazy as _;

from scenery.models import Actor, Place;

from . import managers;

class Title(models.Model):
	""" One printed edition """
	@staticmethod
	def ontology():
		return {
			"keys": {
				"istc_id": "data.cerl.org",
			},
			"sources": {
				"data.cerl.org": {
					"title": "name",
					"date_of_item_single_date": "print_date",
					"date_of_item_from": "print_date",
					"date_of_item_to": "tempus_ante_quem",
					"dimensions": "dimensions",
					"author": "authors",
					"imprint.imprint_name": "printers",
					"imprint.geo_info.geonames_id": "place",
					"gw_ref": "gw_id",
				},
				"heurist.huma-num.fr": {
					"Title": "name",
					"Date": "print_date",
					"Date_ante_quem": "tempus_ante_quem", # Extracted from Date in get_from_heurist_data
					"Place": "place",
					"Historical actor": "authors",
					"Dimension": "dimensions",
					"Editor": "printers",
				},
			},
		};

	objects = managers.TitleManager();

	class Meta:
		verbose_name=_("title");
		verbose_name_plural=_("titles");


	#   ===   Database links   ===
	istc_id = models.CharField(
		_("Incunabala Short Title Catalogue's number"),
		max_length=10,
		null=True, blank=True, unique=True,
		help_text=_("Identifier of this title in the data.cerl.org ISTC database"),
	);
	dishas_id = models.CharField(
		_("Dishas' identifier"),
		max_length = 10,
		null = True, blank = True, unique = True,
		help_text = _("Identifier of the primary source in the disahs.obspm.fr's database"),
	);
	gw_id = models.CharField(
		_("Gesamtkatalog der Weigendrucke's identifier"),
		max_length=10,
		null=True, blank=True, unique=True,
		help_text=_("Identifier of this title in the GW database"),
	);

	#   ===    Intellectual informations   ===
	name = models.TextField(
		_("title"),
		blank=True,
		help_text=_("Title of this edition"),
	);
	authors = models.ManyToManyField(
		Actor,
		verbose_name=_("authors list"),
		related_name="author_of",
		through="Attribution",
		through_fields=("title", "author"),
		help_text=_("List of actors that are considered as authors of this title"),
	);
	printers = models.ManyToManyField(
		Actor,
		verbose_name=_("printers list"),
		related_name="printer_of",
		through="Attribution",
		through_fields=("title", "printer"),
		help_text=_("List of actors that are listed as printers of this title"),
	);

	#   ===   Context of the printing   ===
	place = models.ForeignKey(
		Place,
		verbose_name=_("place"),
		null=True, blank=True,
		on_delete=models.SET_NULL,
		help_text=_("Where was this title printed"),
	);
	print_date = models.IntegerField(
		_("date"),
		null=True, blank=True,
		help_text=_("The year of the printing of this edition."),
	);
	tempus_ante_quem = models.IntegerField(
		_("tempus ante quem"),
		null = True, blank=True,
		help_text = _("The last year when this edition could have been printed (if the tempus ante quem is set, then the other printing date represents the tempus post quem)"),
	);

	#   ===   Material informations   ===
	dimensions = models.CharField(
		_("dimensions"),
		max_length=5,
		blank=True,
		help_text=_("Dimension format of the printed edition (ie 4°, 8°, etc.)"),
	);
	notes = models.TextField(
		_("notes"),
		blank=True,
		help_text = _("Free notes about this title"),
	);

	#   ===   Sources   ===
	data_sources = models.TextField(
		_("sources of the informations"),
		blank=True, default="",
		help_text = _("Source of the informations stored about this title"),
	);

	@property
	def str_print_dates(self) -> str:
		if not self.print_date:
			return "";
		if self.tempus_ante_quem and self.tempus_ante_quem != self.print_date:
			return f"({self.print_date}--{self.tempus_ante_quem})";
		return f"({self.print_date})";

	def __str__(self):
		return f"{self.name} {self.str_print_dates}";

class Attribution(models.Model):
	""" Claim about the authorship of a Title """

	objects = managers.AttributionManager()

	#   ===   Authorship   ===
	author = models.ForeignKey(
		Actor,
		verbose_name=_("author"),
		related_name="alledged_author",
		on_delete = models.CASCADE,
		null=True, blank=True,
		help_text=_("Author attributed to this title"),
	);
	printer = models.ForeignKey(
		Actor,
		verbose_name=_("printer"),
		related_name="alledged_printer",
		on_delete = models.CASCADE,
		null=True, blank=True,
		help_text=_("Alledged printer of this title"),
	);
	title = models.ForeignKey(
		Title,
		verbose_name=_("title"),
		on_delete = models.CASCADE,
		help_text=_("The title on which this authorship assumption was made"),
	);

	#   ===   Sources   ===
	attributor = models.TextField(
		_("source of this attribution"),
		blank=True,
		help_text=_("On which authority (or authorities) was this attribution proposed in the databse"),
	);
	designation = models.TextField(
		_("designation"),
		blank=True,
		help_text=_("How did the attributor authority designate this author or printer for this title"),
	);
	notes = models.TextField(
		_("notes"),
		blank=True,
		help_text=_("Notes about this attribution"),
	);


class Corpus(models.Model):
	""" A way to group together editions of similar works
	(ie. Parisian Alfonsine Tables, Kalendaria, etc.) """

	objects = managers.CorpusManager();

	class Meta:
		verbose_name=_("corpus");
		verbose_name_plural=_("corpuses");


	#   ===   Basic informations   ===
	name = models.CharField(
		_("name"),
		max_length=100,
		help_text=_("Name of this corpus"),
	);
	titles = models.ManyToManyField(
		Title,
		verbose_name=_("titles"),
		related_name="corpus",
		help_text=_("Historical editions included in this corpus"),
	);

	#   ===   Informations on how to build this corpus   ===
	rules = models.TextField(
		_("rules"),
		blank=True,
		help_text=_("Parts of a work title that should indicate it being part of this corpus (each item should be separated by an end of line)"),
	);

	#   ===   Relation between corpuses   ===
	composition_date = models.CharField(
		_("data of composition"),
		max_length = 10,
		blank=True,
		help_text=_("Date when first works of this corpus were composed"),
		default="nd",
	)
	place = models.ForeignKey(
		Place,
		null=True, blank=True,
		on_delete=models.SET_NULL,
		help_text=_("Place where the first works of this corpus were composed"),
	)
	inspired = models.ManyToManyField(
		"self",
		verbose_name=_("inspired corpus"),
		related_name="inspired_by",
		symmetrical = False,
		help_text=_("Other corpuses that were based on works from this one")
	);

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):
		super().save(*args, **kwargs);
		self.add_titles_matching_rules();
		super().save(*args, **kwargs);

	def add_titles_matching_rules(self):
		""" Adds every title in the database that matches the rules of this corpus """
		for title in Title.objects.all():
			if self.does_title_belong(title):
				self.titles.add(title);

	def does_title_belong(self, title: Title) -> bool:
		""" Checks if the given title matches the rules of this corpus """
		for rule in self.rules.split("\n"):
			if rule.lower() in title.name.lower():
				return True;
		return False;
