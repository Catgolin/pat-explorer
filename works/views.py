import networkx as nx;
import matplotlib.pyplot as plt;
import cartopy;
import logging;

from django.shortcuts import render;
from django.views import generic;

from .models import Title, Corpus;
from scenery.models import Actor, Place;

logger = logging.getLogger(__name__)

# HTML Views
class CorpusIndexView(generic.ListView):
	context_object_name = "corpuses_list";

	def get_queryset(self):
		return Corpus.objects.all().order_by("composition_date", "name");

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs);
		context["alone_titles"] = Title.objects.filter(corpus__isnull=True).order_by("print_date");
		return context;


class CorpusDetailView(generic.DetailView):
	model = Corpus;

class TitleDetailView(generic.DetailView):
	model = Title;

class ActorDetailView(generic.DetailView):
	model = Actor;
	template_name = "works/actor_detail.html";


# Image views and animations (for command-line usage)
class DataGraph:
	title_color = "pink"
	title_emp_color = "magenta"
	actor_color = "lightblue"
	place_color = "lightgrey"
	corpus_color = "lightgrey"

	edges_color = "grey"

	title_to_corpus_weight   = 1
	title_to_actor_weight    = 1
	title_to_place_weight    = 1
	corpus_to_corpus_weight  = 1

	nodes = []
	edges = []

	def show_complete_graph(self, prog: str="dot", main_corpus: Corpus=None, **kwargs):
		""" Draws an oriented graph of every titles
		===
		Keyworkd arguments:
		---
		- prog: str="dot"
		  describes the program to use in order to draw the graph.
		  See Graphviz documentation.
		- main_corpus: Corpus=None
		  describes the corpus that should be the center of the attention.
		- with_printers: bool=True
		  tells if the printers should be added to the title's graph
		  (see add_title_to_graph)
		- with_authors: bool=False
		  tells if the authors should be added to the title's graph
		  (see add_title_to_graph)
		- with_place: bool=False
		  tells if the place should be linked to the titles
		  (see add_title_to_graph)
		"""
		self.nodes = []
		self.edges = []
		for title in Title.objects.all():
			if len(title.corpus.all()) < 1:
				continue
			self.add_title_to_graph(title, **kwargs)
		self.emphasis_on_corpus(main_corpus)
		if not main_corpus:
			self.draw_graph(prog)
			return

		root = self.get_corpus_node(main_corpus)
		if root:
			root = root[0]
		self.draw_graph(prog, root)

	def emphasis_on_corpus(self, corpus: Corpus=None):
		""" Updates the color of elements of the specified corpus (if any) """
		if None == corpus:
			return
		for title in corpus.titles.all():
			for node in self.nodes:
				if node == self.get_title_node(title):
					node[1]["color"] = self.title_emp_color


	def add_title_to_graph(self, title, with_printers=True, with_authors=False, with_place=False) -> bool:
		""" Adds a title to a graph, and links it to the nodes that should be associated to it
		Returns True if the title is in the graph at the end """
		if not title or not title.name:
			return False;
		if self.get_title_node(title) in self.nodes:
			return True;

		self.nodes.append(self.get_title_node(title));

		self.add_corpus_from_title(title);

		if with_place:
			self.add_place_from_title(title);

		if with_printers:
			self.add_printers_from_title(title);

		if with_authors:
			self.add_authors_from_title(title);

		return True;

	def add_corpus_from_title(self, title:Title):
		""" Links a title to the corpuses containing it (creates the corpus' node if needed) """
		for corpus in title.corpus.all():
			if self.add_corpus_to_graph(corpus):
				self.add_title_to_corpus_edge(title, corpus);

	def add_place_from_title(self, title: Title):
		""" Links a title to it's printing place (creates the place's node if needed) """
		if not self.add_place_to_graph(title.place):
			self.add_title_to_place_edge(title, title.place)

	def add_printers_from_title(self, title: Title):
		""" Links a title to it's printers (creates the actor's node if needed) """
		for printer in title.printers.all():
			if self.add_actor_to_graph(printer):
				self.add_title_to_actor_edge(title, printer, "Printer")

	def add_authors_from_title(self, title: Title):
		""" Links a title to it's authors (and create the actors' nodes if needed) """
		for author in title.authors.all():
			if self.add_actor_to_graph(author):
				self.add_title_to_actor_edge(title, author, "Author")


	def add_actor_to_graph(self, actor: Actor) -> bool:
		""" Adds an actor to the graph """
		if not actor or not actor.name:
			return False
		if self.get_actor_node(actor) in self.nodes:
			return True

		self.nodes.append(self.get_actor_node(actor))
		return True

	def add_place_to_graph(self, place: Place) -> bool:
		""" Adds an place to the graph """
		if not place or not place.name:
			return False
		if self.get_place_node(place) in self.nodes:
			return True

		self.nodes.append(self.get_place_node(place))
		return True

	def add_corpus_to_graph(self, corpus: Corpus, leave_leaves: bool=False) -> bool:
		""" Adds an corpus to the graph and the corpuses inspired by it
		The leave_leaves optional parameter determines if a corpus without titles should be included """
		if not corpus or not corpus.name:
			return False;
		if self.get_corpus_node(corpus) in self.nodes:
			return True;

		self.nodes.append(self.get_corpus_node(corpus));
		for inspired in corpus.inspired.all():
			if not leave_leaves and inspired.titles.count() < 1:
				continue;
			if not self.add_corpus_to_graph(inspired):
				# The corpus was already added
				continue;
			self.add_corpus_to_corpus_edge(corpus, inspired);
		return True;


	def get_title_node(self, title: Title) -> tuple[str, dict]:
		return ("Title%s" % title.id, {
				"color": self.title_color,
				"label": title.print_date,
		})

	def get_actor_node(self, actor: Actor) -> tuple[str, dict]:
		return ("Actor%s" % actor.id, {
				"color": self.actor_color,
				"label": actor.name.replace(" ", "\n") if None == actor.family_name else actor.family_name,
		})

	def get_place_node(self, place: Place) -> tuple[str, dict]:
		name = place.name
		return ("Place%s" % place.id, {
				"color": self.place_color,
				"label": place.__str__(),
		})

	def get_corpus_node(self, corpus) -> tuple[str, dict]:
		return ("Corpus%s" % corpus.id, {
				"color": self.corpus_color,
				"label": corpus.__str__().replace(" ", "\n"),
		})


	def add_title_to_actor_edge(self, title: Title, actor: Actor, label: str=""):
		title_node = self.get_title_node(title)
		actor_node = self.get_actor_node(actor)
		self.edges.append((
				actor_node[0],
				title_node[0],
				{"weight": self.title_to_actor_weight}
		))

	def add_title_to_place_edge(self, title: Title, place: Place, label: str=""):
		title_node = self.get_title_node(title)
		place_node = self.get_place_node(place)
		self.edges.append((
				title_node[0],
				place_node[0],
				{"weight": self.title_to_place_weight}
		))

	def add_title_to_corpus_edge(self, title: Title, corpus: Corpus, label: str=""):
		title_node = self.get_title_node(title)
		corpus_node = self.get_corpus_node(corpus)
		self.edges.append((
				title_node[0],
				corpus_node[0],
				{"weight": self.title_to_corpus_weight}
		))

	def add_corpus_to_corpus_edge(self, inspiration: Corpus, inspired: Corpus):
		first_node = self.get_corpus_node(inspiration)
		second_node = self.get_corpus_node(inspired)
		self.edges.append((
				first_node[0],
				second_node[0],
				{
						"weight": self.corpus_to_corpus_weight,
						"penwidth": self.corpus_to_corpus_weight,
				}
		))


	def draw_graph(self, prog: str="sfdp", root=None):
		""" Builds the graph and prints it using matplotlib.pyplot """
		old_nodes = [n for n in self.nodes]; # For debugging

		self.remove_duplicate_nodes();
		colors = [prop["color"] for node, prop in self.nodes];
		labels = self.get_labels();

		G = nx.DiGraph();
		G.add_nodes_from(self.nodes);
		G.add_edges_from(self.edges);

		# The following condition is mainly for debuging purposes:
		# It should not do anything if the graph is correctly constructed
		if not self.check_for_errors(old_nodes, G, colors, labels):
			logger.critical("Impossible to print graph");
			return;

		H = nx.nx_agraph.to_agraph(G);
		tree = nx.nx_agraph.graphviz_layout(G, prog=prog, root=root);
		nx.draw(G, pos=tree, with_labels=False, node_color=colors, edge_color=self.edges_color);
		nx.draw_networkx_labels(G, tree, labels);
		plt.show();

	def get_labels(self) -> dict[tuple, str]:
		labels = {};
		for node, prop in self.nodes:
			labels[node] = prop["label"];
		return labels;

	def remove_duplicate_nodes(self):
		tested = {}
		for node in self.nodes:
			if node[0] in tested.keys():
				logger.debug("Removing duplicate node:")
				logger.debug(node)
				logger.debug((node[0], tested[node[0]]))
				self.nodes.remove(node)
			tested[node[0]] = node[1]

	def count_duplicate_nodes(self) -> int:
		tested = {}
		for node in self.nodes:
			if node in tested.keys():
				tested[node]["count"] += 1
			else:
				tested[node] = {
					"node": node,
					"count": 1
				}
		return tested

	def check_for_errors(self, old_nodes, G, colors, labels):
		if len(G) == len(self.nodes) and len(G) == len(colors) and len(G) == len(labels):
			return True
		logger.debug("Graph size (graph): %s" % len(G))
		logger.debug("Graph size (nodes): %s" % len(self.nodes))
		logger.debug("Color number: %s" % len(colors))
		logger.debug("Label number: %s" % len(labels))
		for node in G:
			if not node in [n for n,p in self.nodes]:
				logger.warning("Node of obscure origin: %s" % node)
				occurences = 0
				for n, p in old_nodes:
					if n == node or p == n:
						logger.debug(n,p)
						occurences += 1
				logger.debug("%s occurences before removing duplicates" % occurences)
			elif not node in labels.keys():
				logger.warning("Missing label for %s" % node)
		return False


class GeoData:
	title_color = "red"
	title_emp_color = "purple"
	actor_color = "lightblue"
	source_color = "pink"
	place_color = "lightgrey"
	library_color = "blue"
	corpus_color = "black"

	min_longitude = -8.80789
	min_latitude = 39.74729
	max_longitude = 19.93658
	max_latitude = 51.8661

	fig = None
	ax = None

	# longitude, latitude, name, count
	nodes = []
	edges = []

	def show_corpuses_network(self):
		self.reset_geographic_extents()
		self.set_geographic_projection()
		self.nodes = []
		for corpus in Corpus.objects.all():
			self.draw_corpus_and_relations(corpus)

		plt.tight_layout()
		plt.show()


	def draw_corpus_and_relations(self, corpus):
		origin = self.locate_corpus(corpus)
		if None == origin:
			return

		for destination in corpus.inspired.all():
			self.draw_corpus_relation(corpus, destination)

		self.draw_corpus_point(corpus)

	def draw_corpus_relation(self, origin, destination):
		start = self.locate_corpus(origin)
		end = self.locate_corpus(destination)
		if None == start or None == end:
			return

		self.ax.annotate(
			"",
			xy=(end[0], end[1]),
			xytext=(start[0], start[1]),
			arrowprops=dict(arrowstyle="->"),
			color=self.corpus_color,
		)

		self.draw_corpus_point(destination)

	def draw_corpus_point(self, corpus):
		pos = self.locate_corpus(corpus)
		if None == pos:
			return

		self.ax.plot(
			[pos[0]],
			[pos[1]],
			marker="o",
			color=self.corpus_color,
		)
		self.ax.text(
			pos[0] + 0.1,
			pos[1],
			pos[2]
		)


	def animate_titles_places(self, speed=0.01, start=None, end=None, selected_corpus=None, excluded_corpus=None, size=2):
		if None == start:
			start = self.get_first_title_year()
		if None == end:
			end = self.get_last_title_year()
		self.show_titles_places(
			selected_corpus=selected_corpus,
			excluded_corpus=excluded_corpus,
			start=start,
			end=end,
			size=size,
		)

		self.nodes = []

		self.set_geographic_projection()
		self.update_geographic_extents()

		plt.title(f"{start}")

		plt.get_current_fig_manager().full_screen_toggle()

		plt.tight_layout()

		for year in range(start, end+1):

			plt.title(f"{start}--{year}")

			titles = self.select_titles(
				select=selected_corpus,
				exclude=excluded_corpus
			).filter(print_date = year)
			if titles.count() > 0:
				self.draw_titles_places(titles, size=size)

			plt.pause(speed)

	def show_titles_places(self, selected_corpus=None, excluded_corpus=None, start=None, end=None, size=2):
		self.reset_geographic_extents()
		self.set_geographic_projection()
		self.nodes = []

		titles = self.select_titles(
			select=selected_corpus,
			exclude=excluded_corpus,
			start=start,
			end=end
		);
		self.draw_titles_places(titles, size=size)

		plt.tight_layout()
		plt.show()


	def draw_titles_places(self, titles, size=2):
		for title in titles:
			node = self.get_title_location_node(title)
			if not node:
				continue;
			if node['count'] < 1:
				self.nodes.append(node)
				self.update_geographic_extents(node['longitude'], node['latitude'])
			node['count'] += 1

		for node in self.nodes:
			self.ax.plot(
				[node['longitude']], [node['latitude']],
				marker="o",
				color=self.title_color,
				markersize= size * node['count'],
			)

			if node['drawn']:
				continue

			x = node['longitude'] + 0.2
			y = node['latitude'] - 0.1
			self.ax.text(x, y, node['label'])
			node['drawn'] = True

	def get_title_location_node(self, title):
		data = self.locate_title(title)
		if None == data:
			return None

		for n in self.nodes:
			if (n['longitude'], n['latitude'], n['label']) == data:
				return n
		return {
			'longitude': data[0],
			'latitude': data[1],
			'label': data[2],
			'count': 0,
			'drawn': False
		}


	def show_all_sources(self, corpus=None):
		for title in self.select_titles(corpus, None, None):
			if title.sources.count() > 0:
				self.show_sources_for_title(title)

	def show_sources_for_title(self, title):
		node = self.locate_title(title)
		if None == node:
			return
		ax = plt.axes(projection=cartopy.crs.PlateCarree())
		ax.coastlines()
		self.nodes = [node]
		for source in Source.objects.filter(title=title):
			res = self.locate_source(source)
			if not res:
				continue
			longitude, latitude, name = res
			ax.annotate("", xy=(longitude, latitude), xytext=(node[0], node[1]), arrowprops=dict(arrowstyle="->"))
			ax.plot([longitude], [latitude], marker="o", color=self.source_color)
			self.nodes.append((longitude, latitude, name))
		# self.remove_duplicate_nodes()
		for node in self.nodes:
			ax.text(node[0], node[1], node[2])
		plt.show()


	def locate_corpus(self, corpus):
		if not corpus or not corpus.place:
			return None

		place = corpus.place
		if not place.longitude or not place.latitude:
			return None

		longitude = place.longitude
		latitude = place.latitude

		name = "%(name)s\n%(d)s" % {
			"name": corpus.name,
			"d": corpus.date
		}

		self.update_geographic_extents(longitude, latitude)
		return (longitude, latitude, name)

	def locate_title(self, title):
		if not title or not title.place:
			return None
		place = title.place
		if not place.latitude or not place.longitude:
			return None
		return (place.longitude, place.latitude, place.name)

	def locate_source(self, source):
		if not source or not source.library or not source.library.location:
			return None
		place = source.library.location
		if not place.latitude or not place.longitude:
			return None
		return (place.longitude, place.latitude, place.name)


	def select_titles(self, select=None, exclude=None, start=None, end=None):
		if select:
			titles = select.titles.all()
		else:
			titles = Title.objects.all()

		if exclude:
			excluded = exclude.titles.all();
			titles.difference(excluded);

		if start:
			titles = titles.filter(print_date__gt = start)
		if end:
			titles = titles.filter(print_date__lt = end)

		return titles


	def set_geographic_projection(self, force=True):
		if 'GeoAxesSubplot' == type(self.ax).__name__ and not force:
			return

		self.ax = plt.axes(projection=cartopy.crs.PlateCarree())
		self.ax.coastlines()
		self.ax.stock_img()
		self.ax.add_feature(cartopy.feature.OCEAN)

	def update_geographic_extents(self, longitude=None, latitude=None):
		updated = False
		if None != longitude and longitude < self.min_longitude:
			logger.debug(f"Updating minimum longitude to {longitude}");
			self.min_longitude = longitude
			updated = True
		if None != longitude and longitude > self.max_longitude:
			logger.debug(f"Updating maximum longitude to {longitude}");
			self.max_longitude = longitude
			updated = True
		if None != latitude and latitude < self.min_latitude:
			logger.debug(f"Updating minimum latitude to {latitude}");
			self.min_latitude = latitude
			updated = True
		if None != latitude and latitude > self.max_latitude:
			logger.debug(f"Updating maximum latitude to {latitude}");
			self.max_latitude = latitude
			updated = True

		self.ax.set_extent([
			self.min_longitude - 2,
			self.max_longitude + 2,
			self.min_latitude - 2,
			self.max_latitude + 2
		])

	def reset_geographic_extents(self):
		self.min_longitude = 360
		self.max_longitude = -360
		self.min_latitude = 360
		self.max_latitude = -360

	def get_first_title_year(self):
		return self.get_titles_years_list().filter(print_date__isnull=False).order_by('print_date').first()[0]

	def get_last_title_year(self):
		return self.get_titles_years_list().filter(print_date__isnull=False).order_by('print_date').last()[0]

	def get_titles_years_list(self):
		return Title.objects.values_list('print_date')


class DataChart:
	def show_corpuses_pie(self, label_for_orphans="Sans catégorie"):
		corpus_sizes = [];
		labels = [];
		for corpus in Corpus.objects.all():
			corpus_sizes.append(corpus.titles.count());
			labels.append(corpus.name);
		orphans = Title.objects.filter(corpus__isnull=True);
		corpus_sizes.append(orphans.count());
		labels.append(label_for_orphans);
		total = sum(corpus_sizes);
		plt.pie(
			corpus_sizes,
			labels=labels,
			autopct=lambda p: int(p*total / 100) if p > 0 else None,
		);
		plt.show()

